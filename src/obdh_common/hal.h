/**  
 * @defgroup obdh_hal OBDH Common HAL
 * @{   
 *     @page obdh_hal [OBDH Common HAL]   
 */

#ifndef HAL_H
#define HAL_H

#include <obdh_common/data.h>

/** 
 * @defgroup obdh_hal_power Power Control
 * @{
 * */

/**
 * Turn on 12V power line 
 * */
void power_on_12();
/**
 * Turn off 12V power line 
 * */
void power_off_12();
/**
 * Turn on 7.2V power line 
 * */
void power_on_7();
/**
 * Turn off 7.2V power line 
 * */
void power_off_7();
/**
 * Turn on 5V power line 
 * */
void power_on_5();
/**
 * Turn off 5V power line 
 * */
void power_off_5();

bool is_on_powerline12();
bool is_on_powerline7();
bool is_on_powerline5();
void set_powerline12(bool enable);
void set_powerline7(bool enable);
void set_powerline5(bool enable);

/**@} */

/**
 * Hardware reboot
 * */
void reboot();

/** 
 * @defgroup obdh_hal_time Time
 * @{
 * */

/**
 * Get system time in milliseconds
 * */
uint32_t get_millis();

/**@} */

/** 
 * @defgroup obdh_hal_gps GPS
 * @{
 * */

/**
 * Initialize GPS sensor
 * */
void init_GPS();
/**
 * Acquire data from GPS sensor
 * @param pointer to the gspdata structure
 * @return false in case of reading errors
 * */
bool acquire_GPS(struct GPSData * gpsdata);
/**
 * Enable GPS sensor
 * */
void enable_GPS(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_press Pressure
 * @{
 * */

/**
 * */
void init_PRES();
/**
 *
 * */
uint32_t acquire_PRES();
/**
 * */
void enable_PRES(bool enable);
/**@} */

/** 
 * @defgroup obdh_hal_opc Optic Particle Counter
 * @{
 * */

/**
 *
 * */
void init_OPC();
/** 
 * Acquire available data from OPC.
 * @return Returns true if a complete message is retrieved
 * */
bool acquire_available_OPC();
/**
 * Get the latest complete measurements of OPC
 * @param
 * */
void get_latest_data_OPC(uint16_t * buf38); 
/**
 * Acquire an OPC measurement.
 * Warning: it may take a while
 * @param
 * @return false if read is failed
 * */
bool acquire_OPC(uint16_t * buf19);
/**
 * */
void enable_OPC(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_aic Air Ion Counter
 * @{
 * */

/**
 * */
void init_AIC1();
/**
 *
 * */
uint16_t acquire_AIC1();
/**
 * Enable or disable the fan of AIC1, 
 * useful for AIC offset sampling
 **/
void enable_fan_AIC1(bool enable);
/**
 * */
void enable_AIC1(bool enable);


/**
 * */
void init_AIC2();
/**
 *
 * */
uint16_t acquire_AIC2();
/**
 * Enable or disable the fan of AIC2, 
 * useful for AIC offset sampling
 **/
void enable_fan_AIC2(bool enable);
/**
 * */
void enable_AIC2(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_valves Valves
 * @{
 * */

/**
 * */
void init_VALVES();
/**
 * */
void set_VALVES(uint8_t status);

/**@} */

/** 
 * @defgroup obdh_hal_temp Temperature
 * @{
 * */

/**
 * */
void init_TEMP();
/**
 *
 * */
uint16_t acquire_TEMP();
/**
 * */
void enable_TEMP(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_rh Relative Humudity
 * @{
 * */

/**
 *
 * */
void init_RH();
/**
 * */
uint16_t acquire_RH();
/**
 * */
void enable_RH(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_sd SD Card
 * @{
 * */

/**
 * */
void init_SD();
/**
 *
 * */
void log_SD(struct StatusMessage * msg);
/**
 * */
void enable_SD(bool enable);

/**@} */

/** 
 * @defgroup obdh_hal_pump Pump
 * @{
 * */

/**
 *
 * */
void init_PUMP();
/**
 *
 * */
void set_PUMP(uint16_t value);

/**@} */

/** 
 * @defgroup obdh_hal_com Communications
 * @{
 * */

/**
 * */
void init_GSCOM();
/**
 *
 * @param pointer to the message to send
 * */
void GSCOM_tx(struct Message * msg);
/**
 *
 * @pointer to the data structure where to save the message
 * */
bool GSCOM_rx(struct Message * msg);
/**
 * */
void enable_GSCOM(bool enable);

/**@} */

#endif /* !HAL_H */
/**@}*/
