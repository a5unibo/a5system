/**  
 * @defgroup obdh_data OBDH Common Data 
 * @{   
 *     @page obdh_data [OBDH Common data]   
 */

#ifndef DATA_H
#define DATA_H

#ifdef ARDUINO_CODE
  #include <Arduino.h>
#else
  #include <stdint.h>
#endif


#define MESSAGE_LENGTH(msgptr, length) {  \
  length=sizeof(uint8_t)+sizeof(uint16_t)+ sizeof(uint16_t); \
  switch(msgptr->cmdId){ \
    case STATUS_MSG:    length+=sizeof(struct StatusMessage);break; \
    case SETVAL_MSG:    length+=sizeof(struct SetValueMessage);break; \
    case ACK_MSG:       length+=sizeof(struct AckMessage);break; \
    case CONFREQ_MSG:   length+=sizeof(struct ConfirmReqMessage);break; \
    case CONFRESP_MSG:  length+=sizeof(struct ConfirmResMessage);break; \
    default: break; \
  } \
}


/**
 * GPS values data structure
 * */
struct GPSData {
  /// Latitude (decimal)
	int32_t lat;
  /// Longitude (decimal)
	int32_t lon;
  /// Altitude (meters)
	uint16_t alt;
};


/** 
 * @defgroup helper_to_strings Helper strings
 * @{
 * */
#define StepName(step) (step==PREFLIGHT?std::string("PREFLIGHT"): ( step==IDLE?std::string("IDLE"): ( step==CLIMB?std::string("CLIMB"): ( step==SAMPLING_INIT?std::string("SAMPLING_INIT"): (  step==SAMPLING?std::string("SAMPLING"): ( step==DESCENT?std::string("DESCENT"): ( step==ONGROUND?std::string("ONGROUND"): "UNKNOWN"  )  )  )  )  ) ) )

#define MsgName(id) (id==SETVAL_MSG?std::string("SETVAL_MSG"): ( id==ACK_MSG?std::string("ACK_MSG"): ( id==CONFREQ_MSG?std::string("CONFREQ_MSG"): ( id==CONFRESP_MSG?std::string("CONFRESP_MSG"): (  id==STATUS_MSG?std::string("STATUS_MSG"): "UNKNOWN" )  )  )  ) )
/** @} */

/**
 * Mission steps
 * */
enum Step{
  /// Preflight mission step
  PREFLIGHT=1,
  /// Idle mission step
  IDLE=2,
  /// Climb mission step
  CLIMB=3,
  /// Sampling Initialization mission step
  SAMPLING_INIT=4,
  /// Sampling mission step
  SAMPLING=5,
  /// Descent mission step
  DESCENT=6,
  /// On ground mission step
  ONGROUND=7,
  /// 
  UNKNOWN=0,
};
/**
 * OBDH functional modes
 * */
enum Mode{
  /// Only SetValue messages from OBDH are able to modify HW and change the OBDH state
  MANUAL=1,
  /// OBDH is free to control Hardware, its state is refrected in hw.
  AUTONOMOUS=2,
  /// Undefined mode
  UNDEFINED=0,
};
/**
 * Message types and codes
 * */
enum MsgID{
  /// SetValue uplink message, cmd_id 1
  SETVAL_MSG=1,
  /// Ack downlink message, cmd_id 2
  ACK_MSG=2,
  /// Confirmation request downlink message, cmd_id 3
  CONFREQ_MSG=3,
  /// Confirmation response uplink message, cmd_id 4
  CONFRESP_MSG=4,
  /// Status downling message, cmd_id 5
  STATUS_MSG=5,
  /// Death message
  DEATH_MSG=0x42
};

/** 
 * @defgroup helper_status Helper for status message
 * Helpers for StatusMessage serialization
 * Use it like functions with no return value
 * @{
 * */
#define STATUS_SET_VALVES_OPEN(msg_pointer)   {(msg_pointer)->data.status.flags |= 0x01;}
#define STATUS_SET_GPS_FAULT(msg_pointer) 	  {(msg_pointer)->data.status.flags |= 0x02;}
#define STATUS_SET_OPC_FAULT(msg_pointer) 	  {(msg_pointer)->data.status.flags |= 0x04;}
#define STATUS_SET_GENERAL_FAULT(msg_pointer) {(msg_pointer)->data.status.flags |= 0x08;}
#define STATUS_SET_AIC1_FAN_OPEN(msg_pointer) {(msg_pointer)->data.status.flags |= 0x10;}
#define STATUS_SET_AIC2_FAN_OPEN(msg_pointer) {(msg_pointer)->data.status.flags |= 0x20;}
#define STATUS_SET_OPC_FAN_OPEN(msg_pointer)  {(msg_pointer)->data.status.flags |= 0x40;}

#define STATUS_SET_POWERLINE1_ON(msg_pointer)  {(msg_pointer)->data.status.power |= 0x01;}
#define STATUS_SET_POWERLINE2_ON(msg_pointer)  {(msg_pointer)->data.status.power |= 0x02;}
#define STATUS_SET_POWERLINE3_ON(msg_pointer)  {(msg_pointer)->data.status.power |= 0x04;}

#define STATUS_IS_VALVES_OPEN(msg_pointer) 	  (((msg_pointer)->data.status.flags & 0x01) != 0x00)
#define STATUS_IS_GPS_FAULT(msg_pointer) 	  (((msg_pointer)->data.status.flags & 0x02) != 0x00)
#define STATUS_IS_OPC_FAULT(msg_pointer) 	  (((msg_pointer)->data.status.flags & 0x04) != 0x00)
#define STATUS_IS_GENERAL_FAULT(msg_pointer)  (((msg_pointer)->data.status.flags & 0x08) != 0x00)
#define STATUS_IS_AIC1_FAN_OPEN(msg_pointer)  (((msg_pointer)->data.status.flags & 0x10) != 0x00)
#define STATUS_IS_AIC2_FAN_OPEN(msg_pointer)  (((msg_pointer)->data.status.flags & 0x20) != 0x00)
#define STATUS_IS_OPC_FAN_OPEN(msg_pointer)   (((msg_pointer)->data.status.flags & 0x40) != 0x00)
/**
 * @}
 * */

/**
 * Command ID=5
 * DOWNLINK STATUS datagram
 * */
struct StatusMessage {
 /// TimeStamp in millis
 uint32_t life_signal;
 /// Current Missions Step
 uint8_t state;
 /// Current mode
 uint8_t mode;
 struct GPSData gps_data;
 /// Pressure
 uint16_t press;
 /// Air Ion Counter 1 measure
 uint16_t aic1;
 /// Air Ion Counter 2 measure
 uint16_t aic2;
 /// LOAC measure
 uint16_t opc [38];
 /// Temperature
 uint16_t temp;
 /// Relative humidity
 uint16_t hr;
 /**
  * bit 0 -> VALVES STATUS (open=1)
  * bit 1 -> GPS STATUS (error=1)
  * bit 2 -> OPC STATUS (error=1)
  * bit 3 -> GENERAL STATUS (error=1)
  * bit 4 -> AIC1 FAN (on=1)
  * bit 5 -> AIC2 FAN (on=1) 
  * bit 6 -> OPC FAN (on=1)
  * bit 7 -> unused 
  * */
 uint8_t flags;
 /**
  * bit 0 -> power line 1 (on=1)
  * bit 1 -> power line 1 (on=1)
  * bit 2 -> power line 1 (on=1)
  * */
 uint8_t power; 
} __attribute__((packed));
/**
 * Command ID=2
 * DOWNLINK ACK datagram
 * */
struct AckMessage {
 uint16_t sn_ack;
} __attribute__((packed));
/**
 * Command ID=3
 * DOWNLINK REQUEST CONFIRM datagram
 * */
struct ConfirmReqMessage {
  uint8_t next_state;
} __attribute__((packed));

/** 
 * @defgroup helpers_confirm_resp Helper confirm response message
 * 
 * @{
 * */
#define CONFIRMREQ_YES 15
#define CONFIRMREQ_NO 120
/** 
 * @}
 * */

/**
 * Command ID=4
 * UPLINK RESPONSE datagram
 * */
struct ConfirmResMessage {
 uint8_t next_state_res;  // 15 YES , 120 NO
 uint16_t sn_ref;
}__attribute__((packed));

/** 
 * @defgroup helpers_set_value Helper set value message
 * 
 * @{
 * */
#define SETVALUE_HAVETO_MODIFY_STATE(msg_pointer)   ((msg_pointer)->data.setVal.modify & 0x01 ? true: false)
#define SETVALUE_HAVETO_MODIFY_MODE(msg_pointer)    ((msg_pointer)->data.setVal.modify & 0x02 ? true: false)
#define SETVALUE_HAVETO_MODIFY_PUMP(msg_pointer)    ((msg_pointer)->data.setVal.modify & 0x04 ? true: false)
#define SETVALUE_HAVETO_MODIFY_VALVES(msg_pointer)  ((msg_pointer)->data.setVal.modify & 0x08 ? true: false)
#define SETVALUE_HAVETO_MODIFY_POWER_1(msg_pointer) ((msg_pointer)->data.setVal.modify & 0x10 ? true: false)
#define SETVALUE_HAVETO_MODIFY_POWER_2(msg_pointer) ((msg_pointer)->data.setVal.modify & 0x20 ? true: false)
#define SETVALUE_HAVETO_MODIFY_POWER_3(msg_pointer) ((msg_pointer)->data.setVal.modify & 0x40 ? true: false)

#define SETVALUE_IS_VALVES_OPEN(msg_pointer)        ((msg_pointer)->data.setVal.valves & 0x0f ? true: false)
#define SETVALUE_IS_POWER_ON(msg_pointer)           ((msg_pointer)->data.setVal.modify & 0x80 ? true: false)

#define SETVALUE_SET_STATE(msg_pointer, value) 	{(msg_pointer)->data.setVal.modify = 0x01; (msg_pointer)->data.setVal.state=value;}
#define SETVALUE_SET_MODE(msg_pointer, value)   {(msg_pointer)->data.setVal.modify = 0x02; (msg_pointer)->data.setVal.mode=value;}
#define SETVALUE_SET_PUMP(msg_pointer, value)   {(msg_pointer)->data.setVal.modify = 0x04; (msg_pointer)->data.setVal.pump=value;}
#define SETVALUE_SET_VALVES(msg_pointer, value) {(msg_pointer)->data.setVal.modify = 0x08; (msg_pointer)->data.setVal.valves=(value ? 0x0f : 0xf0);}
/**@} */

/**
 * Command ID=1
 * UPLINK COMMAND datagram
 * */
struct SetValueMessage {
 /** The MODIFY field is a bit that request modification of one or 
  * more of the values of STATE, MODE, PUMP or VALVES. 
  * In particular, its encoding is shown in the following:
  * bit 0 -> STATE
  * bit 1 -> MODE
  * bit 2 -> PUMP
  * bit 3 -> VALVES
  * bit 4 -> POWER LINE 1
  * bit 5 -> POWER LINE 2 
  * bit 6 -> POWER LINE 3
  * bit 7 -> POWER VALUE
  */
 uint8_t modify;
 /// New value for STATE (1 to 6)
 uint8_t state;
 /// New value for MODE (1 to 3)
 uint8_t mode;
 /// New value for PUMP threshold (0 to 255)
 uint8_t pump;
 /// New value for VALVES (B00001111 open, B11110000 close)
 uint8_t valves;
} __attribute__((packed));

/// DEATH MESSAGE OF REBOOT
#define IS_DEATH_MESSAGE(msg_pointer) ( (msg_pointer)->cmdId==DEATH_MSG && (msg_pointer)->sn==0x4242 && (msg_pointer)->crc==0x4242 ? true: false)

/**
 * OBDH message data structure
 * */
struct Message {
   /// Command ID (message type)
   uint8_t cmdId;
   /// Message sequence number
   uint16_t sn;
   /// Message CRC (header+payload)
   uint16_t crc;
   /// Message data (payload)
   union {
      struct StatusMessage status;
      struct AckMessage ack; 
      struct ConfirmReqMessage cReq;
      struct ConfirmResMessage cRes;
      struct SetValueMessage setVal;
   } data;
}__attribute__((packed));

#endif /* !DATA_H */

