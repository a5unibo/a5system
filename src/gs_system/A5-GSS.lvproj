﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="subVI_" Type="Folder">
			<Item Name="bool2disabled.vi" Type="VI" URL="../subVI_A5/bool2disabled.vi"/>
			<Item Name="sub_request.vi" Type="VI" URL="../subVI_A5/sub_request.vi"/>
			<Item Name="sub_acknoledgment.vi" Type="VI" URL="../subVI_A5/sub_acknoledgment.vi"/>
			<Item Name="sub_enum_initialize.vi" Type="VI" URL="../subVI_A5/sub_enum_initialize.vi"/>
			<Item Name="sub_csv_data.vi" Type="VI" URL="../subVI_A5/sub_csv_data.vi"/>
			<Item Name="sub_LOPC.vi" Type="VI" URL="../subVI_A5/sub_LOPC.vi"/>
			<Item Name="sub_GPS.vi" Type="VI" URL="../subVI_A5/sub_GPS.vi"/>
			<Item Name="sub_ELINK_settings.vi" Type="VI" URL="../subVI_A5/sub_ELINK_settings.vi"/>
		</Item>
		<Item Name="subVI_UDP" Type="Folder">
			<Item Name="sub_datagram_1.vi" Type="VI" URL="../subVI_A5/sub_datagram_1.vi"/>
			<Item Name="sub_datagram_4.vi" Type="VI" URL="../subVI_A5/sub_datagram_4.vi"/>
			<Item Name="sub_datagram_6.vi" Type="VI" URL="../subVI_A5/sub_datagram_6.vi"/>
			<Item Name="sub_datagram_FLAGS.vi" Type="VI" URL="../subVI_A5/sub_datagram_FLAGS.vi"/>
			<Item Name="sub_datagram_OPC.vi" Type="VI" URL="../subVI_A5/sub_datagram_OPC.vi"/>
			<Item Name="sub_unpackaging.vi" Type="VI" URL="../subVI_A5/sub_unpackaging.vi"/>
			<Item Name="sub_crc16-ccitt.vi" Type="VI" URL="../subVI_A5/sub_crc16-ccitt.vi"/>
			<Item Name="sub_NoTimeout.vi" Type="VI" URL="../subVI_A5/sub_NoTimeout.vi"/>
		</Item>
		<Item Name="subVI_conversion" Type="Folder">
			<Item Name="str2humidity.vi" Type="VI" URL="../subVI_A5/str2humidity.vi"/>
			<Item Name="str2GPS.vi" Type="VI" URL="../subVI_A5/str2GPS.vi"/>
			<Item Name="str2celsius.vi" Type="VI" URL="../subVI_A5/str2celsius.vi"/>
			<Item Name="byte2char.vi" Type="VI" URL="../subVI_A5/byte2char.vi"/>
			<Item Name="uword2char.vi" Type="VI" URL="../subVI_A5/uword2char.vi"/>
			<Item Name="time2path.vi" Type="VI" URL="../subVI_A5/time2path.vi"/>
		</Item>
		<Item Name="TESTBENCH" Type="Folder">
			<Item Name="TB.vi" Type="VI" URL="../TEST/TB.vi"/>
		</Item>
		<Item Name="var" Type="Folder">
			<Item Name="LOPC.vi" Type="VI" URL="../var/LOPC.vi"/>
			<Item Name="GPS.vi" Type="VI" URL="../var/GPS.vi"/>
		</Item>
		<Item Name="A5-mainGUI.vi" Type="VI" URL="../A5-mainGUI.vi"/>
		<Item Name="logo_GSS.bmp" Type="Document" URL="../../../../../Downloads/logo_GSS.bmp"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
			</Item>
			<Item Name="sub_CRCcalculation.vi" Type="VI" URL="../subVI_A5/sub_CRCcalculation.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="A5unibo GroundStation v1.2" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{0CEC76C7-BF8A-43E9-A5EC-ECADBA788D77}</Property>
				<Property Name="App_INI_GUID" Type="Str">{C8B4AF33-877C-46F2-8B1E-CF1DE42438C5}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8C5DF275-43A0-4E18-81D4-77943A803666}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/A5unibo GroundStation v1.2</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{2807E3FF-A01A-41E6-84DC-C4890A22BA9E}</Property>
				<Property Name="Destination[0].destName" Type="Str">A5unibo GroundStation v1.2.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/A5unibo GroundStation v1.2/A5unibo GroundStation v1.2.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/A5unibo GroundStation v1.2/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{48DFBB4C-F6D3-4B61-BA9A-33453AF7F682}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/A5-mainGUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="TgtF_fileVersion.build" Type="Int">1</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_fileVersion.minor" Type="Int">2</Property>
				<Property Name="TgtF_internalName" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2014 </Property>
				<Property Name="TgtF_productName" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{49BA9624-C795-4BAE-B38F-FE765822816B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">A5unibo GroundStation v1.2.exe</Property>
			</Item>
			<Item Name="A5unibo GroundStation" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">A5unibo GroundStation</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{ACFF8F0E-0C79-43C9-BFA1-032BCDEAF3BE}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{1D4B3111-AF08-4F05-AD81-D476E2D90967}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2012 SP1</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{20385C41-50B1-4416-AC2A-F7D6423A9BD6}</Property>
				<Property Name="DistPartCount" Type="Int">1</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/A5unibo GroundStation/A5unibo GroundStation</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">A5unibo GroundStation</Property>
				<Property Name="INST_defaultDir" Type="Str">{ACFF8F0E-0C79-43C9-BFA1-032BCDEAF3BE}</Property>
				<Property Name="INST_productName" Type="Str">A5unibo GroundStation</Property>
				<Property Name="INST_productVersion" Type="Str">1.2.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">12018005</Property>
				<Property Name="MSI_arpCompany" Type="Str">A5unibo</Property>
				<Property Name="MSI_arpContact" Type="Str">http://www.the5f.com/bexus/</Property>
				<Property Name="MSI_arpURL" Type="Str">A5unibo</Property>
				<Property Name="MSI_distID" Type="Str">{FC8134FE-51EC-456C-B49E-4B8DA8225987}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{6814CED3-3344-47FA-B889-A67F7EDCD2DC}</Property>
				<Property Name="MSI_welcomeImageID" Type="Ref">/My Computer/logo_GSS.bmp</Property>
				<Property Name="MSI_windowMessage" Type="Str">This is the complete installer for A5unibo GroundStation v1.2. It is for stand-alone installation providing also NI Run-time engine.</Property>
				<Property Name="MSI_windowTitle" Type="Str">A5unibo GroundStation</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{ACFF8F0E-0C79-43C9-BFA1-032BCDEAF3BE}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{ACFF8F0E-0C79-43C9-BFA1-032BCDEAF3BE}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">A5unibo GroundStation v1.2.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{49BA9624-C795-4BAE-B38F-FE765822816B}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">A5unibo GroundStation v1.2</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/A5unibo GroundStation v1.2</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
