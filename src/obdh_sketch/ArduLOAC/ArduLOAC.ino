#include <Arduino.h>

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif


/// OPC


#define SERIAL_OBDH Serial1
#define SERIAL_LOAC Serial3

#define OPC_CONCENTRATION(way, chan)      ((float) (way) * _opc_conc_scale[chan%19] )
#define OPC_DNDLOGD(conc)                 ((float) (log10(conc) * 10 )/63 - 0.52 )
#define OPC_START_FRAME 181          // ord(u'µ')
#define OPC_END_FRAME   167          // ord(u'§')
#define OPC_STUPID_FRENCH '\xa3'        // ord(u'£')
#define OPC_BUFFER_SIZE (80*2)
/// Correction scale for counting to concentration conversion using LOAC
const float _opc_conc_scale [19] = { 0.02625,0.0265,0.0286875, 0.0309375, 
  0.034125, 0.0352875, 0.0254375, 0.005635, 0.009375, 0.0075, 0.009375, 
  0.013125, 0.016875, 0.020625, 0.02437 , 0.028125, 0.009375, 0.013125, 
  0.026875 };

enum ParsingPhase{ START=0, READ=1, END=2} _opc_pphase=START;
uint8_t  _opc_read_count=0;
uint8_t  _opc_start_frame_count=0;
uint8_t  _opc_end_frame_count=0;
uint8_t  _opc_read_byte_count=0;
uint8_t  _opc_byte_read=0;
uint16_t _opc_LOAC [40];
uint8_t _opc_data [80];
uint16_t _opc_buf38 [19*2];

void init_OPC() {
  _opc_pphase=START;
  _opc_read_count=0;
  _opc_start_frame_count=0;
  _opc_end_frame_count=0;
  _opc_read_byte_count=0;
  _opc_byte_read=0;
  for (unsigned int i=0;i<19*2;i++){_opc_buf38[i]=0;}

  SERIAL_LOAC.begin(38400);
  SERIAL_LOAC.print("$RESET");     SERIAL_LOAC.print(OPC_STUPID_FRENCH);             // Reset LOAC software
  SERIAL_LOAC.print("$SETTRAMEb"); SERIAL_LOAC.print(OPC_STUPID_FRENCH);             // Setup LOAC's binary mode
  SERIAL_LOAC.print("$SETAUTOm");  SERIAL_LOAC.print(OPC_STUPID_FRENCH);             // Send on request
  return;
}

bool acquire_OPC(uint16_t * buf38){
  bool got_message=false;
  while(SERIAL_LOAC.available()>0){ SERIAL_LOAC.read();} 
  SERIAL_LOAC.print("$TRAME");      SERIAL_LOAC.print(OPC_STUPID_FRENCH);          // Request frame
  DEBUG_MSG("DRIVER is going to read OPC messages");
  while(!got_message){ 
      uint32_t begin=millis();
      while (SERIAL_LOAC.available()==0){
        uint32_t curr=millis();
        if (curr-begin > 250){ 
          ERROR_MSG("DRIVER cannot read OPC messages");
          for (unsigned int i=0; i<38;i++){
            buf38[i]=0xffff;
          }
          return false;
        }
      }
      uint8_t token = SERIAL_LOAC.read();
      switch (_opc_pphase){
        case START: 
          if (token==OPC_START_FRAME){
            _opc_start_frame_count++; 
            if(_opc_start_frame_count==4){
              _opc_pphase=READ;
              _opc_start_frame_count=0;
              _opc_read_byte_count=0;
            }
          } else {
            _opc_start_frame_count=0;
          } 
        break;
        case READ:
          _opc_data[_opc_read_byte_count]=token;
          _opc_read_byte_count++;
          if (_opc_read_byte_count==80){
            _opc_read_byte_count=0;
            _opc_end_frame_count=0;
            _opc_start_frame_count=0;
            _opc_pphase=END;
            for(int i=0, lc=0; i<80 && lc<40;i+=2, lc++){
              _opc_LOAC[lc]= (_opc_data[i+1]<<8) + _opc_data[i];
            }
            for (unsigned int i=2,ch=0; i<40 && ch<19; i+=2, ch++){
               buf38[ch]    =  _opc_LOAC[i];    // */ OPC_CONCENTRATION((_opc_LOAC[i]), ch)  ;  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i]), ch))   * 100;
               buf38[ch+19] =  _opc_LOAC[i+1];  // */ OPC_CONCENTRATION((_opc_LOAC[i+1]), ch);  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i+1]), ch)) * 100;
            }
          } 
        break;
        case END:
          if (token==OPC_END_FRAME){
            _opc_end_frame_count++; 
            if(_opc_end_frame_count==4){
              _opc_pphase=START;
              _opc_end_frame_count=0;
              _opc_start_frame_count=0;
              got_message=true;
              DEBUG_MSG("OPC has received the message");
            }
          } else {
            _opc_end_frame_count=0;
          } 
        break;
      } 
  }
  return true;
}

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  SERIAL_OBDH.begin(38400);
  SERIAL_LOAC.begin(38400);
  
  Serial.setTimeout (150);
  Serial1.setTimeout(150);
  Serial2.setTimeout(150);
  Serial3.setTimeout(150);
  DEBUG_MSG("ARDULOAC Initializing OPC");
  init_OPC();
}

void loop() {
  uint16_t buf38 [38];

  DEBUG_MSG("ARDULOAC waiting for OBDH read request");

  
  while (SERIAL_OBDH.available()==0){
    delay(10);
  }
  uint8_t token = SERIAL_OBDH.read();

  if (token == 0x66){
    bool allok = acquire_OPC(buf38);
    if (!allok){
      ERROR_MSG("LOAC Reading ERROR");
      //init_OPC();
      for (uint8_t i=0; i<38; i++){
        SERIAL_LOAC.print(((uint16_t)0xffff)); 
      }
    } else {
      for (uint8_t i=0; i<38; i++){
        SERIAL_LOAC.print(buf38[i]); 
      }
        
      //Serial.println("WAY1");
      //for(int i = 0;i<19;i++){
      //  Serial.print(buf38[i]);
      //  Serial.print(",");
      //}
      //Serial.println("");
      //Serial.println("WAY2");
      //for(int i = 19;i<38;i++){
      //  Serial.print(buf38[i]);
      //  Serial.print(",");
      //}
    }
  }
}

