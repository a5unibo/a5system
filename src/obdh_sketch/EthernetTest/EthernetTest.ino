#include <Arduino.h>
#include <obdh_common/hal.h>
#include <obdh_driver/driver_consts.h>

#include <obdh_driver/Ethernet/Ethernet.h>
#include <obdh_driver/Ethernet/EthernetUdp.h>

#include <util/crc16.h> // AVR library for CRC
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

//********* HAL implementation
//
//
void power_on_12(){}
void power_off_12(){}
void power_on_7(){}
void power_off_7(){}
void power_on_5(){}
void power_off_5(){}
bool is_on_powerline12(){return true;}
bool is_on_powerline7(){return true;}
bool is_on_powerline5(){return true;}
void set_powerline12(bool enable){}
void set_powerline7(bool enable){}
void set_powerline5(bool enable){}
uint32_t get_millis(){
  return millis();
}
void init_GPS() {
  return;
}
bool acquire_GPS(struct GPSData * gpsdata) {
  return true;
}
void enable_GPS(bool enable) {
  return ;
}
void init_PRES() {
  return;
}
uint32_t acquire_PRES() {
  return 1024;
}
void enable_PRES(bool enable) {
  return;
}
void init_OPC() {
  return;
}
bool acquire_OPC(uint16_t * buf19) {
  return true; 
}
void enable_OPC(bool enable) {
  return;
}
void init_AIC1() {
}
uint16_t acquire_AIC1() {
  return 0;
}
void enable_fan_AIC1(bool enable) {
  return;
}
void enable_AIC1(bool enable) {
  return;
}
void init_AIC2() {
}
uint16_t acquire_AIC2() {
  return 0;
}
void enable_fan_AIC2(bool enable) {
  return;
}
void enable_AIC2(bool enable) {
  return;
}
void init_VALVES(){
}
void set_VALVES(uint8_t status){
}
void init_TEMP() {
  return;
}
uint16_t acquire_TEMP() {
  return (uint16_t) 280;
}
void enable_TEMP(bool enable) {
  return;
}
void init_RH() {
}
uint16_t acquire_RH() {  
   return 80;
}
void enable_RH(bool enable) {
  return;
}
void init_SD() {
  return;
}
void log_SD(struct Message * msg) { 
  return;
}
void enable_SD(bool enable) {
  return;
}
void init_PUMP() {
}
void set_PUMP(uint8_t value) {
}
void reboot() {
}
  
/// COMMUNICATION

uint8_t gscom_mac [6] = GSCOM_MACADDR; 
uint8_t gscom_ip[4] = GSCOM_IP; 
uint8_t gscom_gw[4] = GSCOM_GW; 
uint8_t gscom_mask[4] = GSCOM_MASK; 
DECLARE_GSCOM_GS_IP
DECLARE_GSCOM_OBDH_IP
EthernetUDP UDP;

uint16_t calculateCRC(struct Message * msg, uint16_t len){
	uint16_t x = 0;
	uint16_t pos = 0;
	uint8_t * msgArr = (uint8_t *) msg;
	while(pos < len){
		if(pos>4 || pos<3){
		  //x = _crc_xmodem_update(x, (uint8_t)*msgArr++);
		  pos++;
	  }
	  return x;
  }
  return x;
}

void init_GSCOM() {
	Ethernet.begin(gscom_mac, gscom_obdh_ip);
	UDP.begin(GSCOM_SERVICE_PORT);
}

void GSCOM_tx(struct Message * msg) {
	UDP.beginPacket(gscom_gs_ip, GSCOM_SERVICE_PORT);
	//msg -> crc = calculateCrc((uint8_t *) msg, len);
  int length=0;
  MESSAGE_LENGTH(msg, length);
  Serial.println(String("[-] GSCOM_tx sending message with sn=")+((int)msg->sn)+String(", length=")+length);
  UDP.write((uint8_t *) msg, length);
	UDP.endPacket();
}
bool GSCOM_rx(struct Message * msg) {
  char buffer [512];
  unsigned int length=0;
	int recvPacketSize = UDP.parsePacket();
	if (recvPacketSize > 0){
		UDP.read(buffer , 512);
    MESSAGE_LENGTH(((struct Message *) buffer), length);
    for (unsigned int i=0; i<length && i<sizeof(struct Message); i++){
      ((char*)msg)[i] = buffer[i];
    } 
    Serial.println(String("[-] GSCOM_rx received message with sn=")+((int)msg->sn)+String(", length=")+length);
		return true;
	} else {
		return false;
	}
}

void enable_GSCOM(bool enable) {
	return;
}

//
uint16_t SN = 0; 

void sense_status_and_send() {
  struct GPSData gps_data;
  struct Message msg;
  Step state = PREFLIGHT;
  acquire_GPS(&gps_data);
  acquire_OPC(msg.data.status.opc);
  // AIC1, AIC2, OPC should be red every 10sec.
  uint16_t aic1 = acquire_AIC1();
  uint16_t aic2 = acquire_AIC2();
  // TEMP, PRES, RH may be acquired with higher frequencies    
  uint16_t temp = acquire_TEMP();
  uint16_t pres = (uint16_t) ( acquire_PRES()/100 ); 
  uint16_t rh = acquire_RH();
  uint32_t life_signal = get_millis();

  // Composing message
  msg.cmdId = STATUS_MSG;
  msg.sn = ++SN;
  msg.data.status.life_signal = life_signal;
  msg.data.status.state = state;
  msg.data.status.mode= AUTONOMOUS;
  msg.data.status.gps_data.lat = gps_data.lat;
  msg.data.status.gps_data.lon = gps_data.lon;
  msg.data.status.gps_data.alt = gps_data.alt;
  msg.data.status.press = pres;
  msg.data.status.aic1 = aic1;
  msg.data.status.aic2 = aic2;
  msg.data.status.temp = temp;
  msg.data.status.hr = rh;
  msg.data.status.flags = 0;
  STATUS_SET_VALVES_OPEN(&msg);
  msg.crc = 0; // TODO: calculate CRC
  Serial.println(String("[-] ETHERNET TEST sending status message with sn")+((int)msg.sn));
  log_SD(&msg);
  GSCOM_tx(&msg);
}
void setup(){
  Serial.begin(9600);
  init_GSCOM();
}
void loop() {
  struct Message msg;
  sense_status_and_send();
  if( GSCOM_rx(&msg) ){
    Serial.println(String("[-] ETHERNET TEST message received"));
  }
  delay(1000);
}

