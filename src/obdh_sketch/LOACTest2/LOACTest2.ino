#include <Arduino.h>
#include <obdh_common/hal.h>
#include <obdh_driver/Wire/Wire.h>

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  
  Serial.setTimeout( 150);
  Serial1.setTimeout(150);
  Serial2.setTimeout(150);
  Serial3.setTimeout(150);

  DEBUG_MSG("Initializing OPC");
  init_OPC();
  
}

void loop() {
  uint16_t buf38 [38];
  bool allok = acquire_OPC(buf38);
  if (!allok){
    ERROR_MSG("LOAC Reading ERROR");
    DEBUG_MSG("Initializing OPC");
    init_OPC();
  } else {
      Serial.println("WAY1");
      for(int i = 0;i<19;i++){
        Serial.print(buf38[i]);
        Serial.print(",");
      }
      Serial.println("");
      Serial.println("WAY2");
      for(int i = 19;i<38;i++){
        Serial.print(buf38[i]);
        Serial.print(",");
     }
  }

  delay(8000);
}

