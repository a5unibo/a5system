#include <Arduino.h>
#include <obdh_common/hal.h>
#include <obdh_driver/Wire/Wire.h>

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  
  Serial.setTimeout( 150);
  Serial1.setTimeout(150);
  Serial2.setTimeout(150);
  Serial3.setTimeout(150);
  Wire.begin();
  Wire.setTimeout(250);

  DEBUG_MSG("Initializing pressure");
  init_PRES();
  
}

void loop() {
  uint32_t pres = acquire_PRES();
  DEBUG_MSG("PRESSURE is "<<pres);
  delay(2000);
  DEBUG_MSG("Initializing pressure");
  init_PRES();
}

