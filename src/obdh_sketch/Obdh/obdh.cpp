#include "config.h"
#include <obdh_common/hal.h>
#include <obdh_control/control.h>
#include <Arduino.h>

void check_systems();

void setup() {                
  init_GSCOM();
  init_PRES();
  init_RH();
  init_TEMP();
  init_PUMP();
  init_VALVES();
  init_AIC1();
  init_AIC2();
  init_OPC();
  init_GPS();
  // TODO: start the watchdog
  init_odbh_control();
}

void loop() {
  #ifdef CHECK_SYSTEMS
  check_systems();
  return;
  #else
  // Start control logic main loop
  // NOTE: we decide how frequently run the odbh control function
  run_odbh_control();
  #endif
}

#ifdef CHECK_SYSTEMS
void check_systems(){
  // TODO: HW TEST ROUTINE
}
#endif

int main() {
	setup();
	while(true){loop();}
	return 0;
}
