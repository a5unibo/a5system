/**
 * config.h
 * Copyright (C) A5 Unibo
 *
 * Distributed under terms of the CC-BY-NC license.
 */

#ifndef CONFIG_H
#define CONFIG_H

#define CONTROL_LOOP_PERIOD        250      // ms
#define TIMEOUT_0          ((long) 150)     // Serial   Timeout (ms) 
#define TIMEOUT_1          ((long) 150)     // Serial 1 Timeout (ms)  
#define TIMEOUT_2          ((long) 150)     // Serial 2 Timeout (ms)
#define TIMEOUT_3          ((long) 150)     // Serial 3 Timeout (ms)
#define TIMEOUT_I2C        ((long) 150)     // Timeout I2C (ms)

#endif /* !CONFIG_H */

