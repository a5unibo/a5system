#include "config.h"
#include <Arduino.h>
#include <obdh_common/hal.h>
#include <obdh_control/control.h>
#include <obdh_driver/Wire/Wire.h>

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
  
  DEBUG_MSG("OBDH is powering up"); 
  power_on_5();
  power_on_7();
  power_on_12();
 
  delay(5000);
  
  DEBUG_MSG("OBDH is setting timeouts"); 
 
  Serial.setTimeout( TIMEOUT_0);
  Serial1.setTimeout(TIMEOUT_1);
  Serial2.setTimeout(TIMEOUT_2);
  Serial3.setTimeout(TIMEOUT_3);
  Wire.begin();
  Wire.setTimeout(TIMEOUT_I2C);

  DEBUG_MSG("OBDH is initializing its hardware");
 
  init_GSCOM();
  init_PRES();
  init_RH();
  init_TEMP();
  init_PUMP();
  init_VALVES();
  init_AIC1();
  init_AIC2();
  init_OPC();
  init_GPS();
  init_SD();
  
  DEBUG_MSG("OBDH is initializing its control loop"); 
  init_odbh_control();
}

void loop() {
  // NOTE: we decide how frequently run the odbh control function
  run_odbh_control();
  delay(CONTROL_LOOP_PERIOD);
}

