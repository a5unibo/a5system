#include <Arduino.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

/// OPC

#define OPC_CONCENTRATION(way, chan)      ((float) (way) * _opc_conc_scale[chan%19] )
#define OPC_DNDLOGD(conc)                 ((float) (log10(conc) * 10 )/63 - 0.52 )
#define OPC_START_FRAME 181          // ord(u'µ')
#define OPC_END_FRAME   167          // ord(u'§')
#define OPC_STUPID_FRENCH '\xa3'        // ord(u'£')
#define OPC_BUFFER_SIZE (80*2)
/// Correction scale for counting to concentration conversion using LOAC
const float _opc_conc_scale [19] = { 0.02625,0.0265,0.0286875, 0.0309375, 
  0.034125, 0.0352875, 0.0254375, 0.005635, 0.009375, 0.0075, 0.009375, 
  0.013125, 0.016875, 0.020625, 0.02437 , 0.028125, 0.009375, 0.013125, 
  0.026875 };

enum ParsingPhase{ START=0, READ=1, END=2} _opc_pphase=START;
uint8_t  _opc_read_count=0;
uint8_t  _opc_start_frame_count=0;
uint8_t  _opc_end_frame_count=0;
uint8_t  _opc_read_byte_count=0;
uint8_t  _opc_byte_read=0;
uint8_t  _opc_data [80];
uint16_t _opc_LOAC [40];
uint16_t _opc_buf38 [19*2];

void init_OPC() {
  _opc_pphase=START;
  _opc_read_count=0;
  _opc_start_frame_count=0;
  _opc_end_frame_count=0;
  _opc_read_byte_count=0;
  _opc_byte_read=0;
  DEBUG_MSG("INITIALIZING...")
  for (unsigned int i=0;i<19*2;i++){_opc_buf38[i]=0;}
  Serial1.begin(38400);
  Serial1.print("$RESET");     Serial1.print(OPC_STUPID_FRENCH);                  // Reset LOAC software
  Serial1.print("$SETTRAMEb"); Serial1.print(OPC_STUPID_FRENCH);             // Setup LOAC's binary mode
  Serial1.print("$SETAUTOm");  Serial1.print(OPC_STUPID_FRENCH);             // Send on request
  //Serial1.print("$START10£");                // Send a frame every 10 sec
  //Serial1.print("$TRAME");     Serial1.print(OPC_STUPID_FRENCH);        // Setup LOAC's binary mode
  return;
}
bool acquire_available_OPC(){
  bool got_message=false;
  while(Serial1.available()>0){ 
      uint8_t token = Serial1.read();
      Serial.print(token);
      Serial.println(",");
      switch (_opc_pphase){
        case START: 
          if (token==OPC_START_FRAME){
            _opc_start_frame_count++; 
            if(_opc_start_frame_count==4){
              _opc_pphase=READ;
              _opc_start_frame_count=0;
              _opc_read_byte_count=0;
              got_message=true;
              DEBUG_MSG("START PHASE completed (go READ)");
            }
          } else {
            _opc_start_frame_count=0;
          } 
        break;
        case READ:
          //((uint8_t*)_opc_LOAC)[_opc_read_byte_count]=token; 
          _opc_data[_opc_read_byte_count]=token;
          _opc_read_byte_count++;
          if (_opc_read_byte_count==80){
            Serial.println("");
            DEBUG_MSG("READ PHASE end of read (go END)");
            _opc_read_byte_count=0;
            _opc_end_frame_count=0;
            _opc_start_frame_count=0;
            _opc_pphase=END;
            got_message &= true;
            //return got_message;
          } 
        break;
        case END:
          if (token==OPC_END_FRAME){
            _opc_end_frame_count++; 
            if(_opc_end_frame_count==4){
              _opc_pphase=START;
              _opc_end_frame_count=0;
              _opc_start_frame_count=0;
              DEBUG_MSG("END PHASE completed");
            got_message&=true;
            if (got_message){
              DEBUG_MSG("COMPLETE MESSAGE");
              for(int i=0, lc=0; i<80 && lc<40;i+=2, lc++){
                //_opc_LOAC[lc]= (_opc_data[i]<<8) + _opc_data[i+1];
                _opc_LOAC[lc]= (_opc_data[i+1]<<8) + _opc_data[i];
                //_opc_LOAC[lc]= ((uint16_t*)_opc_data)[i];
                //Serial.print(_opc_data[i]); Serial.print(",");
                //Serial.print(_opc_data[i+1]); Serial.print(",");
              }
              //Serial.println("");
              for (unsigned int i=2,ch=0; i<40 && ch<19; i+=2, ch++){
                 _opc_buf38[ch]    =  _opc_LOAC[i];    // */ OPC_CONCENTRATION((_opc_LOAC[i]), ch)  ;  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i]), ch))   * 100;
                 _opc_buf38[ch+19] =  _opc_LOAC[i+1];  // */ OPC_CONCENTRATION((_opc_LOAC[i+1]), ch);  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i+1]), ch)) * 100;
              }
            }
  
            }
          } else {
            _opc_end_frame_count=0;
            got_message=false;
          } 
        break;
      } 
  }
  return got_message;
}
void get_latest_data_OPC(uint16_t * buf38){  
  for (unsigned int i=0; i<19*2;i++){
    buf38[i]=_opc_buf38[i];
  }
}
void acquire_OPC(uint16_t * buf38){
  uint32_t curr=millis();
  bool got_message=false;
  DEBUG_MSG("Clearing buffer")
  while(Serial1.available()>0){Serial1.read();} 
  DEBUG_MSG("Requesting frame")
  Serial1.print("$TRAME");    Serial1.print(OPC_STUPID_FRENCH);          // Request frame
  DEBUG_MSG("Processing frame")
  while(!got_message){ 
      uint32_t begin=millis();
      while (Serial1.available()==0){
        curr=millis();
        if (curr-begin > 150){ return; }
      }
      uint8_t token = Serial1.read();
      Serial.print(token);
      Serial.println(",");
      switch (_opc_pphase){
        case START: 
          if (token==OPC_START_FRAME){
            _opc_start_frame_count++; 
            if(_opc_start_frame_count==4){
              _opc_pphase=READ;
              _opc_start_frame_count=0;
              _opc_read_byte_count=0;
              DEBUG_MSG("START PHASE completed (go READ)");
            }
          } else {
            _opc_start_frame_count=0;
          } 
        break;
        case READ:
          //((uint8_t*)_opc_LOAC)[_opc_read_byte_count]=token; 
          _opc_data[_opc_read_byte_count]=token;
          _opc_read_byte_count++;
          if (_opc_read_byte_count==80){
            Serial.println("");
            DEBUG_MSG("READ PHASE end of read (go END)");
            _opc_read_byte_count=0;
            _opc_end_frame_count=0;
            _opc_start_frame_count=0;
            _opc_pphase=END;
            DEBUG_MSG("COMPLETE MESSAGE");
            for(int i=0, lc=0; i<80 && lc<40;i+=2, lc++){
              //_opc_LOAC[lc]= (_opc_data[i]<<8) + _opc_data[i+1];
              _opc_LOAC[lc]= (_opc_data[i+1]<<8) + _opc_data[i];
              //_opc_LOAC[lc]= ((uint16_t*)_opc_data)[i];
              //Serial.print(_opc_data[i]); Serial.print(",");
              //Serial.print(_opc_data[i+1]); Serial.print(",");
            }
            //Serial.println("");
            for (unsigned int i=2,ch=0; i<40 && ch<19; i+=2, ch++){
               _opc_buf38[ch]    =  _opc_LOAC[i];    // */ OPC_CONCENTRATION((_opc_LOAC[i]), ch)  ;  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i]), ch))   * 100;
               _opc_buf38[ch+19] =  _opc_LOAC[i+1];  // */ OPC_CONCENTRATION((_opc_LOAC[i+1]), ch);  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i+1]), ch)) * 100;
            }
            
          } 
        break;
        case END:
          if (token==OPC_END_FRAME){
            _opc_end_frame_count++; 
            if(_opc_end_frame_count==4){
              _opc_pphase=START;
              _opc_end_frame_count=0;
              _opc_start_frame_count=0;
              DEBUG_MSG("END PHASE completed");
              got_message=true;
            }
          } else {
            _opc_end_frame_count=0;
          } 
        break;
      } 
  }
  get_latest_data_OPC(buf38);
}
void enable_OPC(bool enable) {
  return;
}


void setup(){
  Serial.begin(38400);
  init_OPC();
}

void loop(){
  uint16_t buffer [19*2];
  delay(10000); 
  uint32_t begin=millis(); 
  //if(acquire_available_OPC()){
      acquire_OPC(buffer);
      uint32_t end=millis(); 
      DEBUG_MSG("OPC read complete (read time = "<<(end-begin)<<"ms)");
      Serial.println("WAY1");
      for(int i = 0;i<19;i++){
        Serial.print(buffer[i]);
        Serial.print(",");
      }
      Serial.println("");
      Serial.println("WAY2");
      for(int i = 19;i<38;i++){
        Serial.print(buffer[i]);
        Serial.print(",");
      }
      Serial.println("");
  //}
}

/*

 */
