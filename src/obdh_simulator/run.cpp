/**
 * run.cpp
 * Copyright (C) Luca Mella <luca.mella@studio.unibo.it>
 *
 * Distributed under terms of the CC-BY-NC license.
 */
#include "mock_hardware.h"
#include "simulator.h"

using namespace obdh_simulator;

int main( int argc, char ** argv){

  Simulator sim;

  init_odbh_control();

  sim.run();

  return 0;
}

