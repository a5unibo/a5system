#include <obdh_simulator/simulator.h>
#include <math.h>

using namespace std;
using namespace obdh_simulator;

/// Simulator implementation

obdh_simulator::Simulator::Simulator() {
  init_mock_hardware(onchange_callback);
  this->per_step_time = obdh_simulator::k_per_step_time;
  this->print_hw_state = true;
  this->print_obdh_state = true;
  this->failure = false;
  this->gs_message_sn = 0;
}
obdh_simulator::Simulator::~Simulator() {
}

void obdh_simulator::Simulator::print_status() {
  if (this->print_hw_state) {
    print_current_hw_state(this->get_hw_status());
  }
  if (this->print_obdh_state) {
    print_current_obdh_state(this->get_obdh_status());
    print_obdh_tasks(this->get_obdh_status(), get_task_list());
  }
}


HardwareStatus* obdh_simulator::Simulator::get_hw_status() {
  // from mock_hardware.cpp
  return get_hw_state();
}
ObdhState* obdh_simulator::Simulator::get_obdh_status() {
  // from control.cpp
  return get_obdh_state();
}

bool obdh_simulator::Simulator::init() {
  // from contron.cpp
  init_odbh_control();
  HardwareStatus * hw_state = this->get_hw_status();
  hw_state->status_press =  press_at_altitude(100); 
  hw_state->status_temp =  temp_at_altitude(100); 
  init_mock_hardware(onchange_callback);
  return true;
}

bool obdh_simulator::Simulator::step() {
  HardwareStatus * hw_state = this->get_hw_status();
  hw_state->status_millis += this->per_step_time;
  run_odbh_control();
  return true;
}

bool obdh_simulator::Simulator::run() {
  HardwareStatus * hw_state = this->get_hw_status();
  ObdhState * obdh_state = this->get_obdh_status();
  struct Message last_status_message;
  struct Message set_msg;
  struct Message ack_msg;
  hw_state->status_press = press_at_altitude(20); 
  hw_state->status_temp = temp_at_altitude(20);

  while (!this->failure) {
    switch (obdh_state->state) {
      case PREFLIGHT:
      	print_current_obdh_state(this->get_obdh_status());
      	print_current_hw_state(this->get_hw_status());
        INFO_MSG("***** preflight: running checklist *****");
        
        struct Message set_msg_for_checklist;
        struct Message ack_msg_for_checklist;
        set_msg_for_checklist.cmdId = SETVAL_MSG;

        // Set Pump ON
        INFO_MSG("preflight: setting Pump ON" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_PUMP(&set_msg_for_checklist, 255);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_message(ACK_MSG, &ack_msg_for_checklist, this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        // Set Pump OFF
        INFO_MSG("preflight: setting Pump OFF" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_PUMP(&set_msg_for_checklist, 0);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_ack(set_msg_for_checklist.sn,&ack_msg_for_checklist, this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        // Set Valves ON
        INFO_MSG("preflight: setting Valves ON" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_VALVES(&set_msg_for_checklist, true);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_message(ACK_MSG, &ack_msg_for_checklist, this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        // Set Valves OFF
        INFO_MSG("preflight: setting Valves OFF" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_VALVES(&set_msg_for_checklist, false);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_ack(set_msg_for_checklist.sn,&ack_msg_for_checklist,this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        
        INFO_MSG("***** preflight: going to start mission *****");
        // Set Autonomous Mode
        INFO_MSG("preflight: setting AUTONOMOUS mode" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_MODE(&set_msg_for_checklist, AUTONOMOUS);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_ack(set_msg_for_checklist.sn,&ack_msg_for_checklist, this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        // Set IDLE State
        INFO_MSG("preflight: setting IDLE state" );
        set_msg_for_checklist.sn = this->gs_message_sn++;
        SETVALUE_SET_STATE(&set_msg_for_checklist, IDLE);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg_for_checklist);
        wait_for_ack(set_msg_for_checklist.sn,&ack_msg_for_checklist, this);
        INFO_MSG("GS received ack message related to ref_sn="<<ack_msg_for_checklist.data.ack.sn_ack);
        
        wait_for_status_msg_w_state(IDLE, &ack_msg_for_checklist, this);
        break;
      case IDLE:
        struct Message creq_msg_for_climb;
        struct Message cresp_msg_for_climb;
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        INFO_MSG("***** idle: waiting to climb *****");

        //DO_LOOP_FOR_SECONDS(60);

        // Now we simulate the takeoff by increasing the altitude
        // from ground to 250 m with avg speed 5 m/s as specified
        // in Bexus user manual
        for (int alt=20; alt<500; alt+=5) {
          hw_state->status_press = press_at_altitude(alt); 
          hw_state->status_temp = temp_at_altitude(alt);
          hw_state->status_gps.alt=alt;
          DO_LOOP_FOR_SECONDS(1);
        }
        if ( !pop_last_message(CONFREQ_MSG, &creq_msg_for_climb) ){
          wait_for_message(CONFREQ_MSG, &creq_msg_for_climb, this);
        }
        INFO_MSG("GS received a state change confirmation request (state="<<StepName(creq_msg_for_climb.data.cReq.next_state)<<":"<<(int)creq_msg_for_climb.data.cReq.next_state<<", sn="<<creq_msg_for_climb.sn<<")");
        cresp_msg_for_climb.cmdId=CONFRESP_MSG;
        cresp_msg_for_climb.sn=++this->gs_message_sn;
        cresp_msg_for_climb.data.cRes.sn_ref=creq_msg_for_climb.sn;
        cresp_msg_for_climb.data.cRes.next_state_res=CONFIRMREQ_YES;
        MOCKHW_SEND_MSG_TO_OBDH(&cresp_msg_for_climb);
        wait_for_status_msg_w_state(CLIMB, &creq_msg_for_climb, this);
        break;
      
      case CLIMB:
        struct Message creq_msg_for_float;
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        INFO_MSG("***** climb: waiting to float *****" );

        set_msg.cmdId = SETVAL_MSG;
        // Set Manual mode
        INFO_MSG("GS is forcing manual mode to OBDH, hw and state should be changed only via SETVAL_MSG");
        set_msg.sn = this->gs_message_sn++;
        SETVALUE_SET_MODE(&set_msg, MANUAL);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg);
        wait_for_ack(set_msg.sn, &ack_msg, this);
        INFO_MSG("SIMULATOR connection down after manaul mode switch, no more answer");
        // Now we simulate the takeoff by increasing the altitude
        // from 500 m with avg speed 5 m/s as specified
        // in Bexus user manual
        for (int alt=500; alt<22000; alt+=5) {
          hw_state->status_press = press_at_altitude(alt); 
          hw_state->status_temp = temp_at_altitude(alt);
          hw_state->status_gps.alt=alt;
          DO_LOOP_FOR_SECONDS(1);
        }
        wait_for_status_msg_w_state(SAMPLING, &creq_msg_for_float, this);
        break;
      case SAMPLING_INIT:   
        // NOTE: This state remain active for a single control loop due to the immediate speed of activation of the valves.
        //       For this reason it is not printed.
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        INFO_MSG("***** sampling_init: waiting for sampling *****");
        DO_LOOP_FOR_SECONDS(10);
        break;
      case SAMPLING:
        struct Message creq_msg_for_descent;
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        INFO_MSG("***** sampling: waiting to descent *****");
        
        
        INFO_MSG("GS is forcing again manual mode to OBDH, because connectivity have been restored and a problem in the OIC has been detected, so valves are going to be closed");
        set_msg.sn = this->gs_message_sn++;
        SETVALUE_SET_MODE(&set_msg, MANUAL);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg);
        wait_for_ack(set_msg.sn, &ack_msg, this);
        
        set_msg.sn = this->gs_message_sn++;
        SETVALUE_SET_VALVES(&set_msg, false);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg);
        wait_for_ack(set_msg.sn, &ack_msg, this);
       
        // Simulate the floating phase: 5 hours as specified by 
        // bexus user manual
        for (int i=0;i<32;i++){    // 550*32 = ~ 5 hours
          DO_LOOP_FOR_SECONDS(550);
          // Dummy heartbeat message
          // In this case we use an unexpected message.
          ack_msg.sn=0; 
          ack_msg.cmdId= ACK_MSG;
          ack_msg.data.ack.sn_ack=0;
          MOCKHW_SEND_MSG_TO_OBDH(&ack_msg);
        }
        
        // Now we simulate the descent by decreasing the altitude
        // from 22000 m with avg speed 7 m/s as specified
        // in Bexus user manual
        for (int alt=22000; alt>17000; alt-=7) {
          hw_state->status_press = press_at_altitude(alt); 
          hw_state->status_temp = temp_at_altitude(alt);
          hw_state->status_gps.alt=alt;
          DO_LOOP_FOR_SECONDS(1);
          if (alt % 30 == 0){
            // Send ack after a while to keep manual mode up.
            ack_msg.sn=0; 
            ack_msg.cmdId= ACK_MSG;
            ack_msg.data.ack.sn_ack=0;
            MOCKHW_SEND_MSG_TO_OBDH(&ack_msg);
          }
        }
        
        INFO_MSG("GS has detected low altitude so we force DESCENT state and manually disable PUMP");
        set_msg.sn = this->gs_message_sn++;
        SETVALUE_SET_PUMP(&set_msg, 0);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg);
        wait_for_ack(set_msg.sn, &ack_msg, this);

        set_msg.sn = this->gs_message_sn++;
        SETVALUE_SET_STATE(&set_msg, DESCENT);
        MOCKHW_SEND_MSG_TO_OBDH(&set_msg);
        wait_for_ack(set_msg.sn, &ack_msg, this);
        
        wait_for_status_msg_w_state(DESCENT, &creq_msg_for_descent, this);
        break;
      case DESCENT:
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        INFO_MSG("***** descent: waiting for on_ground *****"); 
        
        for (int alt=17000; alt>20;alt-=7) {
          hw_state->status_press = press_at_altitude(alt); 
          hw_state->status_temp = temp_at_altitude(alt);
          hw_state->status_gps.alt=alt;
          DO_LOOP_FOR_SECONDS(1);
          if (alt % 30 == 0){
            // Send ack after a while to keep manual mode up.
            ack_msg.sn=0; 
            ack_msg.cmdId= ACK_MSG;
            ack_msg.data.ack.sn_ack=0;
            MOCKHW_SEND_MSG_TO_OBDH(&ack_msg);
          }
        }
        hw_state->status_press = press_at_altitude(20); 
        hw_state->status_temp = temp_at_altitude(20);
        hw_state->status_gps.alt=20;

        DO_LOOP_FOR_SECONDS(1*60*60);

    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
        wait_for_status_msg_w_state(ONGROUND, &creq_msg_for_descent, this);
        break;
      case ONGROUND:
    	  print_current_hw_state(this->get_hw_status());
    	  print_current_obdh_state(this->get_obdh_status());
      	print_obdh_tasks(this->get_obdh_status(), get_task_list());
        
        INFO_MSG("***** on_ground *****");
        
        return true;
      case UNKNOWN:
        return false;
      default:
        return false;
    }

  }
  return true;
}

