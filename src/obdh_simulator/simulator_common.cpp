/**
 * simulator_common.cpp
 * Copyright (C) Luca Mella <luca.mella@studio.unibo.it>
 *
 * Distributed under terms of the CC-BY-NC license.
 */
#include <obdh_simulator/simulator.h>
#include <math.h>

using namespace obdh_simulator;
using namespace std;

/**
 * Temprerature at altitude
 * See http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
 * @param altitude in meters
 * */
uint32_t obdh_simulator::temp_at_altitude(uint32_t altitude) {
  if (altitude>=0 && altitude<1000) return       LINEAR_TEMP(0,     1000,  +20+273, +15+273, altitude);  // +15+273 ;
  if (altitude>=1000 && altitude<2000) return    LINEAR_TEMP(1000,  2000,  +15+273, +8+273 , altitude);  // +8+273  ;
  if (altitude>=2000 && altitude<3000) return    LINEAR_TEMP(2000,  3000,  +8+273 , -4+273 , altitude);  // -4+273  ;
  if (altitude>=3000 && altitude<4000) return    LINEAR_TEMP(3000,  4000,  -4+273 , -11+273, altitude);  // -11+273 ;
  if (altitude>=4000 && altitude<5000) return    LINEAR_TEMP(4000,  5000,  -11+273, -18+273, altitude);  // -18+273 ;
  if (altitude>=5000 && altitude<6000) return    LINEAR_TEMP(5000,  6000,  -18+273, -23+273, altitude);  // -23+273 ;
  if (altitude>=6000 && altitude<7000) return    LINEAR_TEMP(6000,  7000,  -23+273, -31+273, altitude);  // -31+273 ;
  if (altitude>=7000 && altitude<8000) return    LINEAR_TEMP(7000,  8000,  -31+273, -37+273, altitude);  // -37+273 ;
  if (altitude>=8000 && altitude<9000) return    LINEAR_TEMP(8000,  9000,  -37+273, -44+273, altitude);  // -44+273 ;
  if (altitude>=9000 && altitude<10000) return   LINEAR_TEMP(9000,  1000,  -44+273, -50+273, altitude);  // -50+273 ;
  if (altitude>=10000 && altitude<15000) return  LINEAR_TEMP(10000, 15000, -50+273, -57+273, altitude);  // -57+273 ;
  if (altitude>=15000 && altitude<20000) return  LINEAR_TEMP(15000, 20000, -51+273, -51+273, altitude);  // -51+273 ;
  if (altitude>=20000 && altitude<25000) return  LINEAR_TEMP(20000, 25000, -45+273, -45+273, altitude);  // -45+273 ;
  if (altitude>=30000 && altitude<35000) return  LINEAR_TEMP(30000, 35000, -40+273, -40+273, altitude);  // -40+273 ;
  if (altitude>=40000 && altitude<45000) return  LINEAR_TEMP(35000, 45000, -25+273, -25+273, altitude);  // -25+273 ;
  else return -22+273;
}
/**
 * Pressure at altitude
 * See http://www.engineeringtoolbox.com/air-altitude-pressure-d_462.html
 * @param altitude in meters
 *  */
uint32_t obdh_simulator::press_at_altitude(double altitude) { 
  return (uint32_t)(101325 * pow(1-2.25577e-5*altitude, 5.25588));
}


void obdh_simulator::print_message(struct Message * m) {
  switch(m->cmdId){
    case (STATUS_MSG):
      cout << "[-] Status Message (sn="<<m->sn<<"):" << endl;
      cout << "      state            -> " << StepName(m->data.status.state)  << endl;
      cout << "      mode             -> " << (m->data.status.mode==MANUAL?"manual":"autonomous") << endl;
      cout << "      life_signal      -> " << (int)m->data.status.life_signal  << endl;
      cout << "      press            -> " << (int) m->data.status.press       << endl;
      cout << "      temp             -> " << (int) m->data.status.temp        << endl;
      cout << "      rh               -> " << (int) m->data.status.hr          << endl;
      cout << "      opc              -> " << (uint16_t) m->data.status.opc[0]         << endl;
      cout << "      aic1             -> " << (int) m->data.status.aic1        << endl;
      cout << "      aic2             -> " << (int) m->data.status.aic2        << endl;
      cout << "      gps (alt,lat,lon)-> " << (int) m->data.status.gps_data.alt << "," <<(int) m->data.status.gps_data.lat << "," <<(int) m->data.status.gps_data.lon << endl;
      cout << "      valves status    -> " << (STATUS_IS_VALVES_OPEN(m)?"open":"closed") << endl;
      cout << "      gps status       -> " << (STATUS_IS_GPS_FAULT(m)?"fault":"ok")      << endl;
      cout << "      opc status       -> " << (STATUS_IS_OPC_FAULT(m)?"fault":"ok")      << endl;
      cout << "      aic1 fan status  -> " << (STATUS_IS_AIC1_FAN_OPEN(m)?"on":"off")      << endl;
      cout << "      aic1 fan status  -> " << (STATUS_IS_AIC2_FAN_OPEN(m)?"on":"off")      << endl;
      cout << "      opc  fan status  -> " << (STATUS_IS_OPC_FAN_OPEN(m)?"on":"off")      << endl;
      cout << "      general status   -> " << (STATUS_IS_GENERAL_FAULT(m)?"fault":"ok")  << endl;
      break;
    case ACK_MSG:
      cout << "[-] Ack Message (sn="<<m->sn<<"):" << endl;
      cout << "      sn_ack           -> " << m->data.ack.sn_ack<< endl;
      break;
    case SETVAL_MSG:
      cout << "[-] SetValue Message (sn="<<m->sn<<"):" << endl;
      cout << "      have_to_modify_state      -> " << SETVALUE_HAVETO_MODIFY_STATE(m)<<" (state="<<StepName(m->data.setVal.state)<<")"<<endl; 
      cout << "      have_to_modify_mode       -> " << SETVALUE_HAVETO_MODIFY_MODE(m) <<" (mode="<<(m->data.setVal.mode?"auto":"manual")<<")"<<endl; 
      cout << "      have_to_modify_pump       -> " << SETVALUE_HAVETO_MODIFY_PUMP(m)<<" (pump="<<(int)m->data.setVal.pump<<")"<<endl; 
      cout << "      have_to_modify_valves     -> " << SETVALUE_HAVETO_MODIFY_VALVES(m)<<" (valves="<<(SETVALUE_IS_VALVES_OPEN(m)?"open":"close")<<")"<<endl; 
      break;
    case CONFREQ_MSG:
      cout << "[-] Confirm Request Message (sn="<<m->sn<<"):" << endl;
      cout << "      next_step   -> " << StepName(m->data.cReq.next_state)<< endl;
      break;
    case CONFRESP_MSG:
      cout << "[-] Confirm Response Message (sn="<<m->sn<<"):" << endl;
      cout << "      next_step_res    -> " << (m->data.cRes.next_state_res==CONFIRMREQ_YES?"yes":"no")<< endl;
      cout << "      sn_ref      -> " << (int)m->data.cRes.sn_ref<< endl;
      break;
    default:
      cout << "[-] Message sn="<<m->sn<<" (cmd="<<(int)m->cmdId<<")" << endl;
      break;
  }
}
void obdh_simulator::print_current_hw_state(HardwareStatus * hs) {
  cout << "[+] HW Status at " << hs->status_millis << ":" << endl;
  cout << "      valves           -> " <<(int) hs->status_valves <<endl;
  cout << "      pump             -> " <<(int) hs->status_pump << endl;
  cout << "      press            -> " <<(int) hs->status_press << endl;
  cout << "      temp             -> " <<(int) hs->status_temp << endl;
  cout << "      rh               -> " <<(int) hs->status_rh << endl;
  cout << "      opc              -> " <<(int) hs->status_opc << endl;
  cout << "      aic1             -> " <<(int) hs->status_aic1 << endl;
  cout << "      aic2             -> " <<(int) hs->status_aic2 << endl;
  cout << "      gps (alt,lat,lon)-> " <<(int) hs->status_gps.alt << "," <<(int) hs->status_gps.lat << "," <<(int) hs->status_gps.lon << endl;
  cout << "      inbox size       -> " << hs->msg_box.size() << endl;
  if (hs->msg_box.size() > 0) {
  cout << "      inbox latest     -> type=" << (int) hs->msg_box.back()->cmdId << ",sn=" << (int) hs->msg_box.back()->sn << endl;
  }
  cout << "      outbox size      -> " << hs->msg_to_gs.size() << endl;
  if (hs->msg_to_gs.size() > 0) {
  cout << "      outbox latest    -> type=" << (int) hs->msg_to_gs.back()->cmdId << ",sn=" << (int) hs->msg_to_gs.back()->sn << endl;
  }
  cout << "      logbox size      -> " << hs->msg_logged.size() << endl;
  if (hs->msg_logged.size() > 0) {
  cout << "      logbox latest    -> type=" << (int) hs->msg_logged.back()->cmdId << ",sn=" << (int) hs->msg_logged.back()->sn << endl;
  }
}
void obdh_simulator::print_current_obdh_state(ObdhState* os) {
  cout << "[+] Obdh Status at lifesignal=" << os->life_signal << ":" << endl;
  cout << "      State                 -> " << StepName(os->state) << endl;
  cout << "      ManualMode            -> " << (os->manual_mode?"manual":"autonomous") << endl;
  cout << "      StationaryAltFlag     -> " << os->stationary_alt_flag << endl;
  cout << "      IncreasingAltFlag     -> " << os->increasing_alt_flag << endl;
  cout << "      DecreasingAltFlag     -> " << os->decreasing_alt_flag << endl;
  cout << "      GroundAltFlag         -> " << os->ground_alt_flag << endl;
  cout << "      ValvesOpenFlag        -> " << os->valves_open_flag << endl;
  cout << "      ExperimentAltFlag     -> " << os->experiment_alt_flag << endl;
  cout << "      WaitingConfirm        -> " << os->waiting_confirm << ", sn=" << os->resp_sn << endl;
  cout << "      ConfirmReceived       -> " << os->confirm_received << endl;
  cout << "      StateOverrideRequired -> " << os->manual_state_override_req << endl;
  cout << "      ManualStateOverride   -> " << os->manual_state_override << endl;
}
void obdh_simulator::print_obdh_tasks(ObdhState* os, TaskInfo* _tasks) {
  cout << "[+] Obdh Tasks at lifesignal " << os->life_signal << " : " << endl;
  cout << "     - task_enable_for_idle       en=" << _tasks[0].task_enabled  << ",torun=" << _tasks[0].has_to_run  << ",period=" << _tasks[0].period << endl;
  cout << "     - task_enable_for_climb      en=" << _tasks[1].task_enabled  << ",torun=" << _tasks[1].has_to_run  << ",period=" << _tasks[1].period << endl;
  cout << "     - task_enable_for_float      en=" << _tasks[2].task_enabled  << ",torun=" << _tasks[2].has_to_run  << ",period=" << _tasks[2].period << endl;
  cout << "     - task_enable_for_descent    en=" << _tasks[3].task_enabled  << ",torun=" << _tasks[3].has_to_run  << ",period=" << _tasks[3].period << endl;
  cout << "     - task_go_manual             en=" << _tasks[4].task_enabled  << ",torun=" << _tasks[4].has_to_run  << ",period=" << _tasks[4].period << endl;
  cout << "     - task_go_auto               en=" << _tasks[5].task_enabled  << ",torun=" << _tasks[5].has_to_run  << ",period=" << _tasks[5].period << endl;
  cout << "     - task_monitor_altitude      en=" << _tasks[6].task_enabled  << ",torun=" << _tasks[6].has_to_run  << ",period=" << _tasks[6].period << endl;
  cout << "     - task_sense_opc             en=" << _tasks[7].task_enabled  << ",torun=" << _tasks[7].has_to_run  << ",period=" << _tasks[7].period << endl;
  cout << "     - task_sense_status_and_send en=" << _tasks[8].task_enabled  << ",torun=" << _tasks[8].has_to_run  << ",period=" << _tasks[8].period << endl;
  cout << "     - task_poll_for_message      en=" << _tasks[9].task_enabled  << ",torun=" << _tasks[9].has_to_run  << ",period=" << _tasks[9].period << endl;
  cout << "     - task_open_valves           en=" << _tasks[10].task_enabled << ",torun=" << _tasks[10].has_to_run  << ",period=" << _tasks[10].period << endl;
  cout << "     - task_close_valves          en=" << _tasks[11].task_enabled << ",torun=" << _tasks[11].has_to_run << ",period=" << _tasks[11].period << endl;
  cout << "     - task_start_pump            en=" << _tasks[12].task_enabled << ",torun=" << _tasks[12].has_to_run << ",period=" << _tasks[12].period << endl;
  cout << "     - task_stop_pump             en=" << _tasks[13].task_enabled << ",torun=" << _tasks[13].has_to_run << ",period=" << _tasks[13].period << endl;
  cout << "     - task_check_systems         en=" << _tasks[14].task_enabled << ",torun=" << _tasks[14].has_to_run << ",period=" << _tasks[14].period << endl;
  cout << "     - task_ask_confirm_climb     en=" << _tasks[15].task_enabled << ",torun=" << _tasks[15].has_to_run << ",period=" << _tasks[15].period << endl;
  cout << "     - task_ask_confirm_float     en=" << _tasks[16].task_enabled << ",torun=" << _tasks[16].has_to_run << ",period=" << _tasks[16].period << endl;
  cout << "     - task_ask_confirm_descent   en=" << _tasks[17].task_enabled << ",torun=" << _tasks[17].has_to_run << ",period=" << _tasks[17].period << endl;
  cout << "     - task_aic_offset_monitor    en=" << _tasks[18].task_enabled << ",torun=" << _tasks[18].has_to_run << ",period=" << _tasks[18].period << endl;
  cout << "     - task_go_ground             en=" << _tasks[19].task_enabled << ",torun=" << _tasks[19].has_to_run << ",period=" << _tasks[19].period << endl;
}
bool obdh_simulator::onchange_callback(HardwareStatus * status, Component comp) {
  cout << "[+] Hardware has changed "<<ComponentName(comp);
  switch (comp){
    case COMPONENT_PUMP:
      cout<<"- Pump (status="<<(int)status->status_pump<<")"<<endl;
      break;
    case COMPONENT_VALVES:
      cout<<"- Valves (status="<<(int)status->status_valves<<")"<<endl;
      break;
    case COMPONENT_MSG_TX:
    if (status->msg_to_gs.size()>0){
      cout<<"- Message sent to GS: "<<endl;
      struct Message * m = status->msg_to_gs.back();
      print_message(m);
    }
    break;
    case COMPONENT_MSG_RX:
    if (status->msg_box.size()>0){
      cout<<"- Message sent to OBDH: "<<endl;
      struct Message * m = status->msg_box.back();
      print_message(m);
    }
    break;
    default: 
    cout<<endl;
    break;
  }
  return true;
}

bool obdh_simulator::wait_for_status_msg_w_state(Step state, struct Message * msgptr, Simulator * s){
  struct Message tmp; 
  DEBUG_MSG("SIMULATOR is waiting for status message with state "<<StepName(state));
  while (true){
    wait_for_message(STATUS_MSG, &tmp, s);
    if (state==tmp.data.status.state){
      memcpy(msgptr, &tmp, sizeof(struct Message));
      DEBUG_MSG("SIMULATOR has received status message with state "<<StepName(state));
      return true;
    }
  }
}
bool obdh_simulator::wait_for_ack(uint32_t sn_ack, struct Message * msgptr, Simulator * s){
  struct Message tmp; 
  DEBUG_MSG("SIMULATOR is waiting for ack message (sn="<<sn_ack<<")");
  while (true){
    wait_for_message(ACK_MSG, &tmp, s);
    if (sn_ack==tmp.data.ack.sn_ack){
     memcpy(msgptr, &tmp, sizeof(struct Message));
     DEBUG_MSG("SIMULATOR has received ack message (sn="<<sn_ack<<")");
     return true;
    }
  }
}

bool obdh_simulator::pop_last_message(enum MsgID type, struct Message * msgptr){
  HardwareStatus * hs = get_hw_state();
  struct Message * tmp = NULL; 
  for (int i=hs->msg_to_gs.size()-1; i >= 0; i--){
      tmp=hs->msg_to_gs.at(i);
      if (tmp->cmdId==type){
        memcpy(msgptr, get_hw_state()->msg_to_gs.at(i), sizeof(struct Message)); 
        delete get_hw_state()->msg_to_gs.at(i); 
        hs->msg_to_gs.erase(hs->msg_to_gs.begin()+i);
        DEBUG_MSG("SIMULATOR has popped the latest message of type "<<MsgName(type));
        return true;
      }
  } 
  return false; 
}  

bool obdh_simulator::wait_for_message(enum MsgID type, struct Message * msgptr, Simulator * s){
  struct Message * latest = NULL; 
  struct Message * tmp = NULL; 
  DEBUG_MSG("SIMULATOR is waiting for message of type "<<MsgName(type));
  while (true){
    if (get_hw_state()->msg_to_gs.size()>0) {
      get_hw_state()->msg_to_gs.back();
      tmp=get_hw_state()->msg_to_gs.back();
      if (tmp->cmdId==type && ( (latest!=NULL && latest->sn!=tmp->sn && latest->cmdId!=tmp->sn) || latest==NULL) ){
        memcpy(msgptr, get_hw_state()->msg_to_gs.back(), sizeof(struct Message)); 
        delete get_hw_state()->msg_to_gs.back(); get_hw_state()->msg_to_gs.pop_back();
        DEBUG_MSG("SIMULATOR has received a message of type "<<MsgName(type));
        return true;
      }
    }
    latest=tmp;
    s->step();
  }
}

