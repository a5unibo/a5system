#ifndef _H_MOCKHW
#define _H_MOCKHW

/**  
 * @defgroup mock_hw Mock Hardware
 * @{
 */ 
#include <obdh_common/hal.h>
#include <obdh_common/data.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <string>

/** 
 * @defgroup mock_hw_messages Helper mock hw message rx/tx
 * @{
 */   
#define MOCKHW_SEND_MSG_TO_OBDH(message_pointer)  {get_hw_state()->msg_box.push_back((struct Message*) memcpy(new struct Message, message_pointer, sizeof(struct Message)));}
#define MOCKHW_SEND_MSG_TO_GS(message_pointer)    {get_hw_state()->msg_to_gs.push_back((struct Message*) memcpy(new struct Message, message_pointer, sizeof(struct Message)));}
#define MOCKHW_LOG_MSG(message_pointer)           {get_hw_state()->msg_logged.push_back((struct Message*) memcpy(new struct Message, message_pointer, sizeof(struct Message)));}
#define MOCKHW_GET_MSG_TO_GS(message_pointer)     {if (get_hw_state()->msg_to_gs.size()>0){memcpy(message_pointer, get_hw_state()->msg_to_gs.back(), sizeof(struct Message)); delete get_hw_state()->msg_to_gs.back(); get_hw_state()->msg_to_gs.pop_back();} else { std::cerr<<"[w] no message for GS"<<std::endl;}}
/**@} */

/**
 * Mock Hardware status
 * */
struct HardwareStatus{
  uint32_t status_millis;
  uint16_t status_pump;
  uint8_t status_valves;
  uint32_t status_press;
  uint16_t status_aic1;
  uint16_t status_aic2;
  uint16_t status_opc;
  uint16_t status_temp;
  uint16_t status_rh;
  struct GPSData status_gps;

  std::vector<struct Message *> msg_logged;
  std::vector<struct Message *> msg_box;
  std::vector<struct Message *> msg_to_gs;
};

/** 
 * @defgroup mock_hw_string Helper mock hw component strings
 * @{
 */   
#define ComponentName(comp) (comp==COMPONENT_MILLIS?std::string("COMPONENT_MILLIS"): ( comp==COMPONENT_PUMP?std::string("COMPONENT_PUMP"): ( comp==COMPONENT_VALVES?std::string("COMPONENT_VALVES"): ( comp==COMPONENT_PRESS?std::string("COMPONENT_PRESS"): (  comp==COMPONENT_AIC1?std::string("COMPONENT_AIC1"): ( comp==COMPONENT_AIC2?std::string("COMPONENT_AIC2"): ( comp==COMPONENT_OPC?std::string("COMPONENT_OPC"): (comp==COMPONENT_TEMP? std::string("COMPONENT_TEMP") : (comp==COMPONENT_RH?std::string("COMPONENT_RH") : (comp==COMPONENT_MSG_LOG ? std::string("COMPONENT_MSG_LOG"): (comp==COMPONENT_MSG_RX ? std::string("COMPONENT_MSG_RX"): (comp==COMPONENT_MSG_TX? std::string("COMPONENT_MSG_TX"): std::string("UNKNOWN") )) )  )  )  )  )  )  )  ) ) )
/**@} */

/**
 * Mock Hardware components 
 * */
enum Component {
  COMPONENT_MILLIS,
  COMPONENT_PUMP,
  COMPONENT_VALVES,
  COMPONENT_PRESS,
  COMPONENT_AIC1,
  COMPONENT_AIC2,
  COMPONENT_OPC,
  COMPONENT_TEMP,
  COMPONENT_RH,
  COMPONENT_MSG_LOG,
  COMPONENT_MSG_RX,
  COMPONENT_MSG_TX
};


/**
 * Callback function invoked on hardware change
 * */
typedef bool (* onchange_f) (HardwareStatus *, Component source);
/**
 * Initialize the mock hardware.
 * @param callback function to invoce when hardware status changes.
 * @param randomSeed 
 */
void init_mock_hardware(onchange_f callback);
/** 
 * Retrieve current HW status
 * */
HardwareStatus * get_hw_state();

#endif //_H_MOCKHW

/**@} */
