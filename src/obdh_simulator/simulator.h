/**  
 * @defgroup obdh_sim OBDH Simulator 
 * @{   
 *     @page obdh_sim [OBDH Simulator]   
 */

#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <obdh_common/data.h>
#include <obdh_common/hal.h>
#include <obdh_simulator/mock_hardware.h>
#include <obdh_control/control.h>
#include <iostream>

/** 
 * @defgroup obdh_sim_debug Helper debug messages
 * @{
 */   
#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif
/**@}*/

/** 
 * @defgroup obdh_sim_misc Miscellaneous helpers
 * @{
 */   
// Quick and dirty macro to avoid rewrite trivial multiplication everywhere
#define TICKS2TIME(n) ((uint32_t)n * this->per_step_time)

#define DO_LOOP(n) { for (int _zzz=0;_zzz<n;_zzz++){this->step();} };

#define DO_LOOP_FOR_SECONDS( sec ) { DO_LOOP(((int)ceil(sec/(k_per_step_time/1000.0)))); }

#define LINEAR_TEMP(amin,amax,tmin,tmax,alt)  (tmin+(tmin-tmax)*((alt-amin)/(amax-amin)))
/**@} */


namespace obdh_simulator{

  /// Nominal time per control loop in millis
  const uint32_t k_per_step_time=150;
  
  class Simulator {
    public:
      /**
       * Create a Simulator
       * */
      Simulator();
      /**
       * Destroy a simulator
       * */
      ~Simulator();

      /**
       * Initialize the simulation
       * */ 
      bool init();
      /**
       * Do a simulation step
       * */ 
      bool step();
      /**
       * Run the simulation
       * */ 
      virtual bool run();
      /**
       * Access the current hw status
       * */
      HardwareStatus * get_hw_status();
      /**
       * Access the current obdh status
       * */
      ObdhState * get_obdh_status();
      /**
       * Pretty print status info 
       * */
      void print_status();

    protected:
    

      /// Time elapsed at each simulation step (in millis)
      uint32_t per_step_time;
      /// If true print the hw state during simulation 
      bool print_hw_state;
      /// If true print the obdh state during simulation 
      bool print_obdh_state;
      /// Indicate the successful state of the simulation
      bool failure;
      /// Current sequence number for messages sent by simulator
      uint32_t gs_message_sn;

  };

  /** 
   * @defgroup obdh_sim_common Utility functions for simulations
   * @{
   */

  /**
   * Temprerature at altitude
   * See http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
   * @param altitude in meters
   * */
  uint32_t temp_at_altitude(uint32_t altitude);
  /**
   * Pressure at altitude
   * See http://www.engineeringtoolbox.com/air-altitude-pressure-d_462.html
   * @param altitude in meters
   *  */
  uint32_t press_at_altitude(double altitude);
  /** 
   * Onchange Callback
   * @param current mock hardware status
   * @param component which have generated the hardware change
   * */
  bool onchange_callback(HardwareStatus * status, Component comp);
  /**
   * Pretty print a status message
   * @param message to print
   * */
  void print_message(struct Message * m);
  /**
   * Pretty print Hardware State
   * @param Mock Hardware status
   * */
  void print_current_hw_state(HardwareStatus * status);
  /**
   * Pretty print OBDH state
   * @param OBDH state
   * */
  void print_current_obdh_state(ObdhState * state);
  /**
   * Pretty print OBDH tasks
   * @param OBDH state
   * @param OBDH task list
   * */
  void print_obdh_tasks(ObdhState * state, TaskInfo* _tasks);
  /**
   * Pop last message of the specified type
   * @param message type
   * @param message pointer to fill with received message
   * @return true if a message of the specified type is present
   * */
  bool pop_last_message(enum MsgID type, struct Message * msgptr);
  /**
   * Wait for a status message containing specified state
   * @param mission step we are waiting for
   * @param message pointer to fill with received message
   * @param the simulator to use
   * @return true if the status message with specified mission step arrives
   * */ 
  bool wait_for_status_msg_w_state(Step state, struct Message * msgptr, Simulator * s);
  /**
   * Wait for a specific Ack message
   * @param serial number of the message wich we are waiting for
   * @param message pointer to fill with received message
   * @param the simulator to use
   * @return true if the ack message related to the speficied sequence number arrives
   **/
  bool wait_for_ack(uint32_t sn_ack, struct Message * msgptr, Simulator * s);
  /**
   * Wait for a message of a given type 
   * @param message type to monitor 
   * @param message pointer to fill with received message
   * @param the simulator to use
   * @return true if the message specified arrives
   * */
  bool wait_for_message( enum MsgID type, struct Message * msgptr, Simulator * s);

  /**@} */

}
/**@} */

#endif /* !SIMULATOR_H */
