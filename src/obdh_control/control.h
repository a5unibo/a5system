/**  
 * @defgroup obdh_control OBDH Control 
 * @{   
 *     @page obdh_control [OBDH Control Logic]   
 *       ODBH control logic is based on a subset of the Sequential Function Chart (SFC IEC 6113) model. 
 *       Expressivity of SFC is too powerful for our purposes, so a restriction is sufficiently expressive to model 
 *       ODBH control logic, in detail:
 *       - no multiple active state are supported
 *       - no alternative choices, no parallelism, no syncronization, no convergence
 *       - single successor steps
 *       - pulsed, stored, non stored task qualifiers only
 *    
 */  

#ifndef CONTROL_H
#define CONTROL_H

#include <obdh_common/data.h>


/**
 * Type of task supported
 * */
enum TaskType {
  /// Sporadic Task
  SPORADIC=0,
  /// Periodic Task
  PERIODIC=1,
};
/**
 * Task Qualifiers
 * */
enum TaskQualifier {
  /// Task is run once
  PULSED=1,
  /// Task is run according his type one or more time and will 
  /// not be deactived after a state change
  STORED=2,
  /// Task is run according his type one or more time but it will 
  /// be deactived after a state change (not yet supported automatically)
  NONSTORED=3,
};

/// Task Routine function type
typedef void (*TaskRoutine) (void);

/**
 * Task information structure
 * */
struct TaskInfo {
  ///
  enum TaskType type;
  /// 
  enum TaskQualifier qual;
  /// Mission state where task has been enabled.
  enum Step launch_step;
  /// Task period in Millis.
  uint32_t period;
  /// 
  bool task_enabled;
  ///
  bool has_to_run;
  ///
  TaskRoutine task;
  
  uint32_t elapsed_millis;
};
#define CONTROL_EMPTY_TASKINFO { SPORADIC, PULSED, 0, false, false, NULL, 0}

/**
 * OBDH control state
 * */
struct ObdhState {
  // HW status flags
  bool elink_enable;
  bool sd_enable;
  bool gps_enable;
  bool aic1_enable;
  bool aic2_enable;
  bool opc_enable;
  bool temp_enable;
  bool rh_enable;
  bool press_enable;
  bool aic1_fan_enable;
  bool aic2_fan_enable;
  // Sensor status
  /// GPS sensor status
  struct GPSData gps_data ;
  /// AIC1 sensor status
  uint16_t aic1;
  /// AIC2 sensor status
  uint16_t aic2;
  /// @deprecated
  uint64_t opc;
  /// Temperature sensor status
  uint16_t temp;
  /// Pressure sensor status
  uint16_t pres;
  /// Humidity sensor status
  uint16_t rh;
  // Actuotors status
  /// Manual mode status
  bool manual_mode;
  /// Filter valves status
  uint8_t valves_status;
  /// Filter pump status
  uint16_t pump_status;
  // Status flags
  /// Increasing altitude flag
  bool increasing_alt_flag;
  /// Decreasing altitude flag
  bool decreasing_alt_flag;
  /// Stationary altitude flag
  bool stationary_alt_flag;
  /// Above experiment altitude flag
  bool experiment_alt_flag;
  /// Below ground altitude flag
  bool ground_alt_flag;
  /// Filter valves open flag
  bool valves_open_flag;
  /// This field is used by alt monitor routine to count how many time
  ///  the measured altiture complies its expectations
  uint16_t alt_monitor_counter;
  ///  Previous measured altitude in meters
  uint32_t alt_monitor_prev_alt;
  // OBDH Control Status
  /// Current mission status
  enum Step state;
  /// Mission status required by GS via SetValue message
  enum Step manual_state_override; // Message parsing routine must set the desired status here.
  /// Flag indicates a status change request arrived from GS
  bool manual_state_override_req;  // and light on the related flag
  // Message Counters
  /// Life signal counter (millis since startup)
  uint32_t life_signal;
  /// Current message sequence number
  uint16_t message_sn;
  /// Flag indicating the waiting for a confirmation from GS
  bool waiting_confirm;
  /// Time elapsed from latest confirmation request
  uint32_t waiting_confirm_time;
  /// Time elapsed from the arrival of latest message (used in MANUAL_MODE)
  uint32_t connection_timeout_time;
  /// Flag indicating a confirmation message has been received
  bool confirm_received;
  /// Sequence number of the message which has required a confirmation to GS
  uint16_t resp_sn;
  // Time
  /// Timestamp of the control loop entering time
  uint32_t state_enter_time;
  /// Timestamp of the latest control loop
  uint32_t prev_time;
  /// Timestamp of the control loop enter
  uint32_t control_begin;
  /// Timestamp of the control loop exit
  uint32_t control_end;
  /// Elapsed millis from last control loop
  uint32_t elapsed_from_last_control_loop; 
  /// Time elapsed since AIC offset begin
  uint32_t aic_offset_time;
  /// Flag indicating offset has been done
  uint32_t aic_offset_done;
};

#define CONTROL_EMPTY_STATE   { false, false, false, false, false, false, false, false, false, true, true, \
                              {0,0,0}, 0,0,0,0,0,0,false,0,0, \
                              false,false,false,false,false,false, 0, 0, PREFLIGHT, PREFLIGHT, false, \
                              0, 0, false, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, false} 

/**  
 * @defgroup obdh_control_task Task handling functions
 * @{   
 */

/**
 * Pulse a sporadic task.
 * @param Task Information 
 * */
bool task_pulse(TaskInfo * task_info);
/**
 * Store a periodic task.
 * @param Task Information 
 * */
bool task_store(TaskInfo * task_info);
/**
 * Non-Store a periodic task, it will run only in its launch state.
 * @param Task Information 
 * */
bool task_non_store(TaskInfo * task_info);
/**
 * Reset, turn off a stored periodic task.
 * @param Task Information 
 * */
bool task_reset(TaskInfo * task_info);
/**@} */


/**  
 * @defgroup obdh_control_fsa Finite State Automa functions
 * @{   
 */
/**
 * Retrieve the next step.
 * */
enum Step fsa_query_next(void);
/**
 * @param active step to deactivate
 * */
bool fsa_deactivate(enum Step step);
/**
 * @param step to activate
 * */
bool fsa_activate(enum Step step);
/**@} */


/**  
 * @defgroup obdh_control_logic Control logic functions
 * @{   
 */
/**
 * Initialize tasks and OBDH state
 * */
void init_odbh_control();
/**
 * Run a OBDH control loop
 * */
void run_odbh_control();
/**
 * Retrieve the current OBDH state
 * */
ObdhState * get_obdh_state();
/**
 * Retrieve the current OBDH task list
 * */
TaskInfo* get_task_list();

/**@}*/

#endif /* !CONTROL_H */

/**
 * @}   
 */  
