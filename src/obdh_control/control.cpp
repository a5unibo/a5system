/**
 * control.cpp
 * Copyright (C) A5 Unibo 
 *
 * Distributed under terms of the CC-BY-NC license.
 */

#include "control.h"
#include <obdh_common/hal.h>

#ifdef ARDUINO_CODE
  #include <Arduino.h>
#else
  #include <math.h>
#endif

#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

/// Constants

#define CONNECTION_TIMEOUT_MANUAL_MODE ((int32_t) 60000)  /** OBDH should receive at least a message every 1 min, 
                                                             * otherwise connection is assumed as down when manual 
                                                             * mode is on.
                                                             * */
#define WAIT_CONFIRM_TIME_LIMIT_MSEC ((int32_t) 60000)  /** 1 min of wait time in case of state change 
                                                          *  confirmation request
                                                          * */
#define WAIT_AIC_OFFSET_LIMIT_MSEC ((int32_t) 180000)  /** 3 min of aic offset time
                                                        * */
#define MINIMUL_PUMP_VALUE         200 
#define PUMP_SAMPLING_VALUE        380 

#define ICAO_STD_ATMO_0KM_TEMP     15       /** C*/	
#define ICAO_STD_ATMO_0KM_PRES     1013.25  /** hPa*/
#define ICAO_STD_ATMO_11KM_TEMP    -56.5    /** C*/	
#define ICAO_STD_ATMO_11KM_PRES    226.00   /** hPa*/
#define ICAO_STD_ATMO_20KM_TEMP    -56.5    /** C*/	
#define ICAO_STD_ATMO_20KM_PRES    54.70    /** hPa*/
#define ICAO_STD_ATMO_32KM_TEMP    -44.5    /** C*/	
#define ICAO_STD_ATMO_32KM_PRES    8.68     /** hPa*/

#define EXPERIMENT_GROUND_PRESSURE   ((uint32_t) 97000)    /** Pa */   // 1000 mbar ~ 500 m
#define EXPERIMENT_SHUTDOWN_PRESSURE ((uint32_t) 16500)    /** Pa */   // 165 mbar ~ 13000 m
#define EXPERIMENT_TARGET_PRESSURE   ((uint32_t)  5500)    /** Pa */   // 55  mbar ~ 20000 m
#define ALT_MONITOR_COUNT_THR      10       /** This mean that if altitude monitor measure "ALT_MONITOR_COUNT_THR" 
                                             * compliant values it turns on altitude flags. 
                                             * Consider that monitor_altitude task should be run every 200ms, 
                                             * so 1000 * 10 = 10000 ms 
                                             * */

#define AIR_GAS_CONSTANT           286.9              /** J/Kg K (SI Units)*/
#define GRAVITY_ACCELL             9.81               /** ms^2   (SI Units)*/
#define PRESSURE_ALTITUDE_CONST    (AIR_GAS_CONSTANT/GRAVITY_ACCELL) 
#define LN_P0                      11.52608845149651  /** ln(ICAO_STD_ATMO_0KM_PRES)  Pascal*/

/// Utility Functions

#define C2K(celsius) (celsius+273.15)

/**
 * Note that task execution priorities are encoded 
 * in their position in the task vector
 * */
#define MAX_TASK_NUM 21
#define task_enable_for_idle            (_tasks[0])
#define task_enable_for_climb           (_tasks[1])
#define task_enable_for_sampling_init   (_tasks[2])
#define task_enable_for_sampling        (_tasks[3])
#define task_enable_for_descent         (_tasks[4])
#define task_go_manual                  (_tasks[5])
#define task_go_auto                    (_tasks[6])
#define task_monitor_altitude           (_tasks[7])
#define task_sense_opc                  (_tasks[8])
#define task_sense_status_and_send      (_tasks[9])
#define task_poll_for_message           (_tasks[10])
#define task_open_valves                (_tasks[11])
#define task_close_valves               (_tasks[12])
#define task_start_pump                 (_tasks[13])
#define task_stop_pump                  (_tasks[14])
#define task_check_systems              (_tasks[15])
#define task_ask_confirm_climb          (_tasks[16])
#define task_ask_confirm_float          (_tasks[17])
#define task_ask_confirm_descent        (_tasks[18])
#define task_aic_offset_monitor         (_tasks[19])
#define task_go_ground                  (_tasks[20])

void enable_for_idle();
void enable_for_climb();
void enable_for_sampling_init();
void enable_for_sampling();
void enable_for_descent();
void go_onground();
void sense_status_and_send();
void poll_for_message();
void open_valves();
void close_valves();
void start_pump();
void stop_pump();
void check_systems();
void go_manual();
void go_auto();
void ask_confirm_climb();
void ask_confirm_float();
void ask_confirm_descent();
void monitor_altitude();
void aic_offset_monitor();
void sense_opc();

/// State of the OBDH control logic
struct ObdhState _state = CONTROL_EMPTY_STATE;
/// Task container
struct TaskInfo _tasks[MAX_TASK_NUM];

/// TASK FUNCTIONS

bool task_pulse(TaskInfo * task_info) {
  task_info->task_enabled = true;
  task_info->has_to_run = true;
  return true;
}
bool task_store(TaskInfo * task_info) {
  task_info->task_enabled = true;
  task_info->has_to_run = true;
  return true;
}
bool task_non_store(TaskInfo * task_info) {
  task_info->task_enabled = true;
  task_info->has_to_run = true;
  return true;
}
bool task_reset(TaskInfo * task_info) {
  task_info->task_enabled = false;
  task_info->has_to_run = false;
  return true;
}

/// FSA FUNCTIONS

enum Step fsa_query_next() {
  enum Step next_state = _state.state;

  switch (_state.state) {
    case PREFLIGHT:
      break;
    case IDLE:
      if (_state.increasing_alt_flag && _state.confirm_received) {
        next_state = CLIMB;
      }
      break;
    case CLIMB:
      if (_state.stationary_alt_flag && _state.experiment_alt_flag
          && _state.confirm_received) {
        next_state = SAMPLING_INIT;
      }
      break;
    case SAMPLING_INIT:
      if (_state.valves_open_flag && _state.aic_offset_done) {
        next_state = SAMPLING;
      }
      break;
    case SAMPLING:
      if (_state.decreasing_alt_flag && !_state.experiment_alt_flag
          && _state.confirm_received) {
        next_state = DESCENT;
      }
      break;
    case DESCENT:
      if (_state.stationary_alt_flag && _state.ground_alt_flag) {
        next_state = ONGROUND;
      }
      break;
    case ONGROUND:
      break;
    case UNKNOWN:
      break;
  }
  return next_state;
}
bool fsa_deactivate(enum Step step) {
  switch (step) {
    case PREFLIGHT:
      _state.confirm_received = false;
      _state.resp_sn = 0;
      _state.waiting_confirm_time = 0;
      _state.waiting_confirm = false;
      task_reset(&task_check_systems);
      break;
    case IDLE:
      _state.confirm_received = false;
      _state.resp_sn = 0;
      _state.waiting_confirm_time = 0;
      _state.waiting_confirm = false;
      task_reset(&task_go_auto);
      task_reset(&task_enable_for_idle);
      task_reset(&task_ask_confirm_climb);
      break;
    case CLIMB:
      _state.confirm_received = false;
      _state.resp_sn = 0;
      _state.waiting_confirm_time = 0;
      _state.waiting_confirm = false;
      task_reset(&task_enable_for_climb);
      task_reset(&task_ask_confirm_float);
      break;
    case SAMPLING_INIT:
      //_state.confirm_received = false;
      //_state.resp_sn = 0;
      //_state.waiting_confirm_time = 0;
      //_state.waiting_confirm = false;
      _state.aic_offset_done=false;
      _state.aic_offset_time=0;
      task_reset(&task_aic_offset_monitor);
      task_reset(&task_enable_for_sampling_init);
      break;
    case SAMPLING:
      _state.confirm_received = false;
      _state.resp_sn = 0;
      _state.waiting_confirm_time = 0;
      _state.waiting_confirm = false;
      task_reset(&task_enable_for_sampling);
      task_reset(&task_open_valves);
	    task_reset(&task_start_pump);
      task_reset(&task_ask_confirm_descent);
      break;
    case DESCENT:
      task_reset(&task_stop_pump);
      task_reset(&task_close_valves);
      task_reset(&task_enable_for_descent);
      break;
    case ONGROUND:
      task_reset(&task_go_ground);
      break;
    case UNKNOWN:
      break;
  }
  return true;
}
bool fsa_activate(enum Step step) {
  if (step == _state.state) {
    return false;
  }

  switch (step) {
    case PREFLIGHT:
      task_pulse(&task_check_systems);
      task_store(&task_poll_for_message);
      task_store(&task_sense_status_and_send);
      task_store(&task_monitor_altitude);
      //task_store(&task_sense_opc);
      break;
    case IDLE:
      task_pulse(&task_go_auto);
      task_pulse(&task_enable_for_idle);
      task_non_store(&task_ask_confirm_climb);
      break;
    case CLIMB:
      task_pulse(&task_enable_for_climb);
      task_non_store(&task_ask_confirm_float);
      break;
    case SAMPLING_INIT:
      task_non_store(&task_aic_offset_monitor);
      task_pulse(&task_enable_for_sampling_init);
      task_pulse(&task_open_valves);
      break;
    case SAMPLING:
	    task_pulse(&task_open_valves);
      task_store(&task_start_pump);
      task_pulse(&task_enable_for_sampling);
      task_non_store(&task_ask_confirm_descent);
      break;
    case DESCENT:
      task_pulse(&task_enable_for_descent);
      task_reset(&task_start_pump);
      task_pulse(&task_stop_pump);
      task_pulse(&task_close_valves);
      break;
    case ONGROUND:
      task_pulse(&task_go_ground);
      break;
    case UNKNOWN:
      break;
  }
  return true;
}

// CONTROL LOGIC FUNCTION

void init_odbh_control() {

  //TODO: read last state from SD_CARD and set it

  // Initialization of task information 
  for (int i = 0; i < MAX_TASK_NUM; i++) {
    _tasks[i].task_enabled = false;
    _tasks[i].has_to_run = false;
    _tasks[i].period = 0;
    _tasks[i].type = SPORADIC;
    _tasks[i].qual = PULSED;
  }

  task_enable_for_idle.type = SPORADIC;
  task_enable_for_idle.qual = PULSED;
  task_enable_for_idle.task = &enable_for_idle;

  task_enable_for_climb.type = SPORADIC;
  task_enable_for_climb.qual = PULSED;
  task_enable_for_climb.task = &enable_for_climb;

  task_enable_for_sampling_init.type = SPORADIC;
  task_enable_for_sampling_init.qual = PULSED;
  task_enable_for_sampling_init.task = &enable_for_sampling_init;
  
  task_enable_for_sampling.type = SPORADIC;
  task_enable_for_sampling.qual = PULSED;
  task_enable_for_sampling.task = &enable_for_sampling;

  task_enable_for_descent.type = SPORADIC;
  task_enable_for_descent.qual = PULSED;
  task_enable_for_descent.task = &enable_for_descent;

  //task_sense_opc.type = PERIODIC;
  //task_sense_opc.period = 2000;
  //task_sense_opc.qual = STORED;
  //task_sense_opc.task_enabled = true;
  //task_sense_opc.has_to_run = true;
  //task_sense_opc.task = &sense_opc;

  task_sense_status_and_send.type = PERIODIC;
  task_sense_status_and_send.period = 1000*10;
  task_sense_status_and_send.qual = STORED;
  task_sense_status_and_send.task_enabled = true;
  task_sense_status_and_send.has_to_run = true;
  task_sense_status_and_send.task = &sense_status_and_send;

  task_poll_for_message.type = PERIODIC;
  task_poll_for_message.period = 500; // TODO
  task_poll_for_message.task_enabled = true;
  task_poll_for_message.has_to_run = true;
  task_poll_for_message.qual = STORED;
  task_poll_for_message.task = &poll_for_message;

  task_open_valves.type = SPORADIC;
  task_open_valves.period = 0; // TODO
  task_open_valves.qual = NONSTORED;
  task_open_valves.task = &open_valves;

  task_close_valves.type = SPORADIC;
  task_close_valves.period = 0; // TODO
  task_close_valves.qual = NONSTORED;
  task_close_valves.task = &close_valves;

  task_start_pump.type = PERIODIC;
  task_start_pump.period = 200; // TODO
  task_start_pump.qual = NONSTORED;
  task_start_pump.task = &start_pump;

  task_stop_pump.type = SPORADIC;
  task_stop_pump.qual = PULSED;
  task_stop_pump.task = &stop_pump;

  task_check_systems.type = SPORADIC;
  task_check_systems.period = 5000; // TODO
  task_check_systems.qual = PULSED;
  task_check_systems.task = &check_systems;

  task_go_manual.type = SPORADIC;
  task_go_manual.qual = PULSED;
  task_go_manual.task = &go_manual;

  task_go_auto.type = SPORADIC;
  task_go_auto.qual = PULSED;
  task_go_auto.task = &go_auto;

  task_ask_confirm_climb.type = PERIODIC;
  task_ask_confirm_climb.period = 500; // TODO
  task_ask_confirm_climb.qual = NONSTORED;
  task_ask_confirm_climb.task = &ask_confirm_climb;

  task_ask_confirm_float.type = PERIODIC;
  task_ask_confirm_float.period = 500; // TODO
  task_ask_confirm_float.qual = NONSTORED;
  task_ask_confirm_float.task = &ask_confirm_float;

  task_ask_confirm_descent.type = PERIODIC;
  task_ask_confirm_descent.period = 500; // TODO
  task_ask_confirm_descent.qual = NONSTORED;
  task_ask_confirm_descent.task = &ask_confirm_descent;

  task_monitor_altitude.type = PERIODIC;
  task_monitor_altitude.period = 1000; // TODO
  task_monitor_altitude.task_enabled = true;
  task_monitor_altitude.has_to_run = true;
  task_monitor_altitude.qual = STORED;
  task_monitor_altitude.task = &monitor_altitude;
  
  task_aic_offset_monitor.type = PERIODIC;
  task_aic_offset_monitor.period = 5000;
  task_aic_offset_monitor.task_enabled = false;
  task_aic_offset_monitor.has_to_run = false;
  task_aic_offset_monitor.qual = NONSTORED;
  task_aic_offset_monitor.task = &aic_offset_monitor;
  
  task_go_ground.type = SPORADIC;
  task_go_ground.qual = PULSED;
  task_go_ground.task = &go_onground;

  // Initial status
  _state.state = PREFLIGHT;
  _state.life_signal = 0;
  _state.manual_mode = false;
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = true;
  _state.aic2_enable = true;
  _state.opc_enable = true;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = MINIMUL_PUMP_VALUE;
  _state.valves_status = 0;
  _state.aic1_fan_enable = true;
  _state.aic2_fan_enable = true;
  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
}

void run_odbh_control() {
  enum Step next_state;
  _state.prev_time = _state.control_end;
  _state.control_begin = get_millis();
  _state.life_signal = _state.control_begin;
  _state.elapsed_from_last_control_loop = _state.control_begin - _state.prev_time;
  _state.state_enter_time += _state.elapsed_from_last_control_loop;
  
  // Check for connection down
  if (_state.manual_mode){
    _state.connection_timeout_time+=_state.elapsed_from_last_control_loop;
    if (_state.connection_timeout_time > CONNECTION_TIMEOUT_MANUAL_MODE){
      ERROR_MSG("OBDH is disabling manual mode due to lack of connectivity with GS (timeout "<< CONNECTION_TIMEOUT_MANUAL_MODE / 1000 <<"s)");
      _state.manual_mode=false;
      _state.connection_timeout_time=0;
    }
  }

  // Activate tasks
  for (int i = 0; i < MAX_TASK_NUM; i++) {
    if (_tasks[i].task_enabled) {
      switch (_tasks[i].type) {
        case SPORADIC:
          if (_tasks[i].has_to_run) {
            _tasks[i].task();
            _tasks[i].has_to_run = false;
          }
          break;
        case PERIODIC:
          _tasks[i].elapsed_millis += _state.elapsed_from_last_control_loop;
          if (_tasks[i].has_to_run && _tasks[i].period <= _tasks[i].elapsed_millis) {
            _tasks[i].task();
            _tasks[i].has_to_run = true;
            _tasks[i].elapsed_millis = 0;
          }
          break;
      }
    }
  }


  // Determining next state considering GS override request and OBDH state 
  if (_state.manual_state_override_req) {
    INFO_MSG("OBDH is switching to the STATE required by GS");
    next_state = _state.manual_state_override;
    _state.manual_state_override_req = false;
  } else {
    if (!_state.manual_mode){
      // next mission step is queried only in AUTONOMOUS mode
      next_state = fsa_query_next();
    } else {
      next_state=_state.state;
    }
  }

  // State transitions
  if (next_state != _state.state) {
    // state change happens in both AUTONOMOUS and MANUAL mode
    fsa_deactivate(_state.state);
    fsa_activate(next_state);
    _state.state=next_state;
    _state.state_enter_time=0;
  }
  _state.control_end = get_millis();
}

ObdhState * get_obdh_state() {
  return &_state;
}
TaskInfo* get_task_list() {
  return _tasks;
}

/// STATE PREPARATION TASKS

void enable_for_idle() {
  INFO_MSG("OBDH is preparing hw for IDLE");
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = true;
  _state.aic2_enable = true;
  _state.opc_enable = true;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = MINIMUL_PUMP_VALUE;
  _state.valves_status = 0;
  _state.aic1_fan_enable = false;
  _state.aic2_fan_enable = false;

  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
  if (!_state.manual_mode){
  }
}
void enable_for_climb() {
  INFO_MSG("OBDH is preparing hw for CLIMB");
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = true;
  _state.aic2_enable = true;
  _state.opc_enable = true;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = MINIMUL_PUMP_VALUE;
  _state.valves_status = 0;
  _state.aic1_fan_enable = true;
  _state.aic2_fan_enable = true;

  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
  if (!_state.manual_mode){
  }  
}
void enable_for_sampling_init() {
  INFO_MSG("OBDH is preparing hw for SAMPLING_INIT");
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = true;
  _state.aic2_enable = true;
  _state.opc_enable = true;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = MINIMUL_PUMP_VALUE;
  _state.valves_status = 255;
  _state.valves_open_flag=true;
  _state.aic1_fan_enable = true;
  _state.aic2_fan_enable = true;

  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
}
void enable_for_sampling() {
  INFO_MSG("OBDH is preparing hw for SAMPLING");
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = true;
  _state.aic2_enable = true;
  _state.opc_enable = true;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = PUMP_SAMPLING_VALUE;
  _state.valves_status = 255;
  _state.valves_open_flag=true;
  _state.aic1_fan_enable = true;
  _state.aic2_fan_enable = true;

  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
}
void enable_for_descent() {
  INFO_MSG("OBDH is preparing hw for DESCENT");
  _state.elink_enable = true;
  _state.sd_enable = true;
  _state.gps_enable = true;
  _state.aic1_enable = false;
  _state.aic2_enable = false;
  _state.opc_enable = false;
  _state.temp_enable = true;
  _state.rh_enable = true;
  _state.press_enable = true;
  _state.pump_status = MINIMUL_PUMP_VALUE;
  _state.valves_status = 0;
  _state.valves_open_flag=false;
  _state.aic1_fan_enable = false;
  _state.aic2_fan_enable = false;

  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);
}

void go_onground() {
  ERROR_MSG("OBDH is preparing to hit the GROUND (non reversible, shutdown!)");
  _state.elink_enable = true;
  _state.sd_enable = false;
  _state.gps_enable = true;
  _state.aic1_enable = false;
  _state.aic2_enable = false;
  _state.opc_enable = false;
  _state.temp_enable = false;
  _state.rh_enable = false;
  _state.press_enable = true;
  _state.pump_status = 0;
  _state.valves_status = 0;
  _state.valves_open_flag=false;
  _state.aic1_fan_enable = false;
  _state.aic2_fan_enable = false;

  enable_fan_AIC1(_state.aic1_fan_enable);
  enable_fan_AIC2(_state.aic2_fan_enable);
  set_VALVES(_state.valves_status);
  set_PUMP(_state.pump_status);
  enable_GSCOM(_state.elink_enable);
  enable_SD(_state.sd_enable);
  enable_AIC1(_state.aic1_enable);
  enable_AIC2(_state.aic2_enable);
  enable_OPC(_state.opc_enable);
  enable_GPS(_state.gps_enable);
  enable_PRES(_state.press_enable);
  enable_TEMP(_state.temp_enable);
  enable_RH(_state.rh_enable);

  power_off_12();
  power_off_7();
  power_off_5();
}

/// STATUS MONITORING AND LOG TASK

void sense_status_and_send() {
  struct Message msg;
  bool gps_ok=false, opc_ok=false;
  // AIC1, AIC2, OPC should be red every 10sec.
  _state.life_signal = get_millis();
  opc_ok=acquire_OPC(msg.data.status.opc); // <-- synchronous, blocking
  _state.pres = (uint16_t) ( acquire_PRES()/10 ); // FIX of issue #23, Pascal to mbar
  _state.temp = acquire_TEMP();
  _state.aic1 = acquire_AIC1();
  _state.aic2 = acquire_AIC2();
  _state.rh = acquire_RH();
  gps_ok=acquire_GPS(&_state.gps_data);

  // Composing message
  msg.cmdId = STATUS_MSG;
  msg.sn = ++_state.message_sn;
  msg.data.status.life_signal = _state.life_signal;
  msg.data.status.state = _state.state;
  msg.data.status.mode= (_state.manual_mode?MANUAL:AUTONOMOUS);
  msg.data.status.gps_data.lat = _state.gps_data.lat;
  msg.data.status.gps_data.lon = _state.gps_data.lon;
  msg.data.status.gps_data.alt = _state.gps_data.alt;
  msg.data.status.press = _state.pres;
  msg.data.status.aic1 = _state.aic1;
  msg.data.status.aic2 = _state.aic2;
  msg.data.status.temp = _state.temp;
  msg.data.status.hr = _state.rh;
  msg.data.status.flags = '\x00';
  
  if (_state.valves_open_flag) {
    STATUS_SET_VALVES_OPEN(&msg);
  }
  if (_state.aic1_fan_enable) {
    STATUS_SET_AIC1_FAN_OPEN(&msg);
  }
  if (_state.aic2_fan_enable) {
    STATUS_SET_AIC2_FAN_OPEN(&msg);
  }
  if (!opc_ok){
    STATUS_SET_OPC_FAULT(&msg);
  }
  if (!gps_ok){
    STATUS_SET_GPS_FAULT(&msg);
  }
  msg.data.status.power='\x00';
  if (is_on_powerline12()){
    STATUS_SET_POWERLINE1_ON(&msg); 
  } 
  if (is_on_powerline7()){
    STATUS_SET_POWERLINE2_ON(&msg); 
  } 
  if (is_on_powerline5()){
    STATUS_SET_POWERLINE3_ON(&msg); 
  } 
  msg.crc = 0; // CRC should be calculated by the driver
  DEBUG_MSG("OBDH is sensing, logging and sending home measurements (sn="<<(int)msg.sn<<" @"<<_state.life_signal<<")");
  log_SD( &(msg.data.status) );
  GSCOM_tx(&msg);
}

/// COMMUNICATION AND CONTROL TASKS

void poll_for_message() {
  struct Message msg;

  // Poll for a single message or poll and serve messages within a maximum time ?
  //DEBUG_MSG("OBDH is polling messages");
  if (GSCOM_rx(&msg)) {

    // Reset connection timeout (HeartBeat)
    if (_state.manual_mode){
      _state.connection_timeout_time=0;
    }

    switch (msg.cmdId) {
      case DEATH_MSG:
        DEBUG_MSG("OBDH death message received");
        if (IS_DEATH_MESSAGE(&msg)){
          DEBUG_MSG("OBDH is coing to reboot");
          reboot();
        }
        break;
      case SETVAL_MSG:
        if (SETVALUE_HAVETO_MODIFY_STATE(&msg) && msg.data.setVal.state != _state.state) {
          INFO_MSG("OBDH received a modify state message ("<<(enum Step) msg.data.setVal.state<<")");
          _state.manual_state_override = (enum Step) msg.data.setVal.state;
          _state.manual_state_override_req = true;
        }
        if ( SETVALUE_HAVETO_MODIFY_MODE(&msg)) {
          INFO_MSG("OBDH received a manual mode request (manual_mode="<<( msg.data.setVal.mode==MANUAL?"on":"off")<<")");
          _state.manual_mode = (msg.data.setVal.mode==MANUAL);
        }
        if (SETVALUE_HAVETO_MODIFY_PUMP(&msg)) {
          _state.pump_status = msg.data.setVal.pump*4;   // pump value domain in message is 0-255, so multiplying it *4 we obtain 0-1020 (pump domain 0-1024)
          INFO_MSG("OBDH received a pump modification request ("<<_state.pump_status<<")");
          set_PUMP(_state.pump_status);
        }
        if (SETVALUE_HAVETO_MODIFY_VALVES(&msg)) {
          INFO_MSG("OBDH received a valves modification request ("<<(int)msg.data.setVal.valves<<")");
          if (SETVALUE_IS_VALVES_OPEN(&msg)){
            _state.valves_open_flag=true;
            _state.valves_status = 255;
          } else {
            _state.valves_open_flag=false;
            _state.valves_status = 0;
          }
          set_VALVES(_state.valves_status);
        }
        if (SETVALUE_HAVETO_MODIFY_POWER_1(&msg)){
          DEBUG_MSG("OBDH power message for line 1 "<<(SETVALUE_IS_POWER_ON(&msg)));
          set_powerline12(SETVALUE_IS_POWER_ON(&msg));
        }
        if (SETVALUE_HAVETO_MODIFY_POWER_2(&msg)){
          DEBUG_MSG("OBDH power message for line 2 "<<(SETVALUE_IS_POWER_ON(&msg)));
          set_powerline7(SETVALUE_IS_POWER_ON(&msg));
        }
        if (SETVALUE_HAVETO_MODIFY_POWER_3(&msg)){
          DEBUG_MSG("OBDH power message for line 3 "<<(SETVALUE_IS_POWER_ON(&msg)));
          set_powerline5(SETVALUE_IS_POWER_ON(&msg));
        }
        // Send ack message
        msg.cmdId = ACK_MSG;
        msg.data.ack.sn_ack = msg.sn;
        msg.sn = ++_state.message_sn;
        msg.crc = 0; // TODO: calculate CRC
        DEBUG_MSG("OBDH is sending back ACK with sn="<<(int)msg.sn<<"");
        GSCOM_tx(&msg);
        break;
      case STATUS_MSG:
        ERROR_MSG("ODBH should not receive an STATUS UPDATE message");
        break;
      case CONFREQ_MSG:
        ERROR_MSG("ODBH should not receive a CONFIRM REQUEST message");
        break;
      case CONFRESP_MSG:
        if (!_state.confirm_received
            && _state.resp_sn == msg.data.cRes.sn_ref) {
          INFO_MSG("ODBH has received the CONFIRM_RESPONSE for request with sn="<<msg.data.cRes.sn_ref);
          if (msg.data.cRes.next_state_res == CONFIRMREQ_YES){
            _state.confirm_received = true;
          } else {
            // If unconfirmed reset the confirmation timeout.
            // This mean the GS should keep negating the state change
            ERROR_MSG("OBDH, GS does not confirm the state change, resetting timeouts. (CONFIRM_RESPONSE sn="<<msg.data.cRes.sn_ref<<")");
            _state.waiting_confirm_time=0;
          }
        }
        break;
      case ACK_MSG:
        // ERROR: ODBC should not receive an ACK message
        ERROR_MSG("[!] ODBH should not receive an ACK message");
        break;
      default:
        ERROR_MSG("[!] OBDH received an unsupported message");
        break;
    }
  }
}

/// VALVES CONTROL TASK

void open_valves() {
  if (!_state.manual_mode) {
    _state.valves_status = 255;
    _state.valves_open_flag = true;
    DEBUG_MSG("ODBH is opening valves ("<<(int)_state.valves_status<<")");
    set_VALVES(_state.valves_status);
  }
}
void close_valves() {
  if (!_state.manual_mode) {
    _state.valves_open_flag = false;
    _state.valves_status = 0;
    DEBUG_MSG("ODBH is closing valves ("<<(int)_state.valves_status<<")");
    set_VALVES(_state.valves_status);
  }
}

/// PUMP CONTROL TASKS

void start_pump() {
  if (!_state.manual_mode) {
    _state.pump_status = PUMP_SAMPLING_VALUE;
    //DEBUG_MSG("ODBH is starting pump ("<<(int)_state.pump_status<<")");
    set_PUMP(_state.pump_status);
  }
}
void stop_pump() {
  if (!_state.manual_mode) {
    _state.pump_status = MINIMUL_PUMP_VALUE;//0;
    DEBUG_MSG("ODBH is closing pump ("<<(int)_state.pump_status<<")");
    set_PUMP(_state.pump_status);
  }
}

/// MISC TASKS

void check_systems() {
  // TODO: check something
  _state.manual_mode = true;
}
void go_manual() {
  DEBUG_MSG("ODBH is switching in manual mode (no changes in HW despite control logic except explicit SET_VALUE messages)");
  _state.manual_mode = true;
}
void go_auto() {
  DEBUG_MSG("ODBH is switching in autonomous mode");
  _state.manual_mode = false;
}

/// ASK CONFIRM TASKS

void ask_confirm_climb() {
  struct Message msg;

  if (_state.increasing_alt_flag && !_state.experiment_alt_flag  && !_state.waiting_confirm) {
    INFO_MSG("ODBH is asking for CLIMB state switching confirmation");
    _state.waiting_confirm = true;
    _state.waiting_confirm_time = 0;
    msg.cmdId = CONFREQ_MSG;
    msg.sn = ++_state.message_sn;
    _state.resp_sn = msg.sn;
    msg.data.cReq.next_state = CLIMB;
    GSCOM_tx(&msg);
  } else {
    if (_state.waiting_confirm) {
      _state.waiting_confirm_time += task_ask_confirm_climb.period;
    }
  }

  if (_state.waiting_confirm_time > WAIT_CONFIRM_TIME_LIMIT_MSEC) {
    ERROR_MSG("ODBH has not received any CLIMB state confirmation for long time (no connection? manual_mode="<<_state.manual_mode<<")");
    _state.waiting_confirm_time = 0;
    _state.waiting_confirm = false;
    _state.confirm_received = true;
  }
}
void ask_confirm_float() {
  struct Message msg;

  if (_state.stationary_alt_flag && _state.experiment_alt_flag && !_state.waiting_confirm) {
    INFO_MSG("ODBH is asking for SAMPLINT_INIT state switching confirmation");
    _state.waiting_confirm = true;
    _state.waiting_confirm_time = 0;
    msg.cmdId = CONFREQ_MSG;
    msg.sn = ++_state.message_sn;
    _state.resp_sn = msg.sn;
    msg.data.cReq.next_state = SAMPLING_INIT;
    GSCOM_tx(&msg);
  } else {
    if (_state.waiting_confirm) {
      _state.waiting_confirm_time += task_ask_confirm_float.period; 
    }
  }
  if (_state.waiting_confirm_time > WAIT_CONFIRM_TIME_LIMIT_MSEC) {
    ERROR_MSG("ODBH has not received any SAMPLING_INIT state confirmation for long time (no connection? manual_mode="<<_state.manual_mode<<")");
    _state.waiting_confirm_time = 0;
    _state.waiting_confirm = false;
    _state.confirm_received = true;
  }

}
void ask_confirm_descent() {
  struct Message msg;

  if (_state.decreasing_alt_flag && !_state.experiment_alt_flag  && !_state.waiting_confirm) {
    INFO_MSG("ODBH is asking for DESCENT state switching confirmation");
    _state.waiting_confirm = true;
    _state.waiting_confirm_time = 0;
    msg.cmdId = CONFREQ_MSG;
    msg.sn = ++_state.message_sn;
    _state.resp_sn = msg.sn;
    msg.data.cReq.next_state = DESCENT;
    GSCOM_tx(&msg);
  } else {
    if (_state.waiting_confirm) {
      _state.waiting_confirm_time += task_ask_confirm_descent.period; 
    }
  }
  if (_state.waiting_confirm_time > WAIT_CONFIRM_TIME_LIMIT_MSEC) {
    ERROR_MSG("ODBH has not received any DESCENT state confirmation for long time (no connection? manual_mode="<<_state.manual_mode<<")");
    _state.waiting_confirm_time = 0;
    _state.waiting_confirm = false;
    _state.confirm_received = true;
  }
}
void monitor_altitude() {
  // Pressure altitude  = c * T * ln( P0 / Pz)
  // See http://www.hills-database.co.uk/altim.html
  //uint16_t temp = acquire_TEMP()/100;   // ASSUMPTION: temperature in Kelvin
  // From Pressure to Density
  uint32_t pres = acquire_PRES();   // ASSUMPTION: pressure in Pascal
  // ASSUMPTION: humidity error is negligible
  // ASSUMPTION: gravity error is negligible
  uint32_t pressure_altitude = pres; //(uint16_t) (PRESSURE_ALTITUDE_CONST * temp * ( LN_P0 - log(pres)));  // ln - ln
  

  // Altitude levels handling
  if (pressure_altitude >= EXPERIMENT_GROUND_PRESSURE){
    _state.ground_alt_flag = true;
  } else {
    _state.ground_alt_flag = false;
  }
  if (pressure_altitude <= EXPERIMENT_TARGET_PRESSURE){
    _state.experiment_alt_flag=true;
  } else {
    _state.experiment_alt_flag=false;
  }
 
  switch (_state.state) {
    case PREFLIGHT:
      break;
    case IDLE:
      if (!_state.ground_alt_flag && _state.alt_monitor_prev_alt > pressure_altitude) {
        _state.alt_monitor_counter++;
      } else { 
        if (_state.alt_monitor_prev_alt < pressure_altitude){
          // You are going down
          _state.alt_monitor_counter=0;
        }
      }
      if (_state.alt_monitor_counter > ALT_MONITOR_COUNT_THR) {
        INFO_MSG("ODBH altitude is increasing (CLIMB, alt="<<(int)pressure_altitude<<")");
        _state.stationary_alt_flag = false;
        _state.decreasing_alt_flag = false;
        _state.increasing_alt_flag = true;
        _state.alt_monitor_counter = 0;
      }
      break;
    case CLIMB:
      if (_state.experiment_alt_flag) {
        _state.alt_monitor_counter++;
      } else { 
        _state.alt_monitor_counter=0;
      }
      if (_state.alt_monitor_counter > ALT_MONITOR_COUNT_THR) {
        INFO_MSG("ODBH altitude is over experiment target altitude (SAMPLING_INIT/SAMPLING, alt="<<(int)pressure_altitude<<")");
        _state.stationary_alt_flag = true;
        _state.decreasing_alt_flag = false;
        _state.increasing_alt_flag = false;
        _state.alt_monitor_counter = 0;
      }
      break;
    case SAMPLING_INIT:
    case SAMPLING:
      if (!_state.experiment_alt_flag && _state.alt_monitor_prev_alt < pressure_altitude) {
        _state.alt_monitor_counter++;
      } else { 
        if (_state.alt_monitor_prev_alt > pressure_altitude){
          // You are going up
          _state.alt_monitor_counter=0;
        }
      }
      if (_state.alt_monitor_counter > ALT_MONITOR_COUNT_THR) {
        INFO_MSG("ODBH altitude is decreasing below experiment target altitude (DESCENT, alt="<<(int)pressure_altitude<<")");
        _state.stationary_alt_flag = false;
        _state.decreasing_alt_flag = true;
        _state.increasing_alt_flag = false;
        _state.alt_monitor_counter = 0;
      }
      break;
    case DESCENT:
      if (pressure_altitude > ((uint32_t)EXPERIMENT_SHUTDOWN_PRESSURE)) {
        _state.alt_monitor_counter++;
      } else { 
        _state.alt_monitor_counter=0;
      }
      if (_state.alt_monitor_counter > ALT_MONITOR_COUNT_THR ) {
        INFO_MSG("ODBH altitude is (almost) on  ground (ON_GROUND, alt="<<(int)pressure_altitude<<")");
        _state.stationary_alt_flag = true;
        _state.ground_alt_flag = true;
        _state.decreasing_alt_flag = false;
        _state.increasing_alt_flag = false;
        _state.ground_alt_flag=true;
        _state.alt_monitor_counter = 0;
      }
      break;
    case ONGROUND:
    case UNKNOWN:
      _state.stationary_alt_flag = false;
      _state.decreasing_alt_flag = false;
      _state.increasing_alt_flag = false;
      _state.alt_monitor_counter = 0;
      break;
  }
  //DEBUG_MSG("ODBH altitude (alt="<<pressure_altitude<<" @ "<<_state.life_signal<<", #match="<<_state.alt_monitor_counter<<",temp="<<temp<<",pres="<<pres<<")");
  _state.alt_monitor_prev_alt = pressure_altitude;
}

void aic_offset_monitor() {
  if (!_state.aic_offset_done){
    _state.aic_offset_time+=task_aic_offset_monitor.period;
      _state.aic1_fan_enable = false;
      _state.aic2_fan_enable = false;
      enable_fan_AIC1(_state.aic1_fan_enable);
      enable_fan_AIC2(_state.aic2_fan_enable);
    if (_state.aic_offset_time > WAIT_AIC_OFFSET_LIMIT_MSEC) {
      INFO_MSG("ODBH has finished the offset");
      _state.aic_offset_time = 0;
      _state.aic_offset_done = true;
      _state.aic1_fan_enable = true;
      _state.aic2_fan_enable = true;
      enable_fan_AIC1(_state.aic1_fan_enable);
      enable_fan_AIC2(_state.aic2_fan_enable);
    }
  }
}

void sense_opc() {
 if (_state.opc_enable){
    if (acquire_available_OPC()){
      INFO_MSG("OPC message received");
    } else {
      DEBUG_MSG("Sensing OPC")
    }
 }  
}
