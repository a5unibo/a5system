#include <obdh_common/hal.h>
#include <obdh_common/data.h>
#include <obdh_driver/driver_consts.h>

#include <Arduino.h>
#include <math.h>
#include <obdh_driver/Ethernet/Ethernet.h>
#include <obdh_driver/Ethernet/EthernetServer.h>
#include <obdh_driver/Ethernet/EthernetClient.h>


#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

#define LINEAR_TEMP(amin,amax,tmin,tmax,alt)  (tmin+(tmin-tmax)*((alt-amin)/(amax-amin)))
/**
 * Temprerature at altitude
 * See http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
 * @param altitude in meters
 * */
uint32_t temp_at_altitude(uint32_t altitude) {
  if (altitude>=0 && altitude<1000) return       LINEAR_TEMP(0,     1000,  +20+273, +15+273, altitude);  // +15+273 ;
  if (altitude>=1000 && altitude<2000) return    LINEAR_TEMP(1000,  2000,  +15+273, +8+273 , altitude);  // +8+273  ;
  if (altitude>=2000 && altitude<3000) return    LINEAR_TEMP(2000,  3000,  +8+273 , -4+273 , altitude);  // -4+273  ;
  if (altitude>=3000 && altitude<4000) return    LINEAR_TEMP(3000,  4000,  -4+273 , -11+273, altitude);  // -11+273 ;
  if (altitude>=4000 && altitude<5000) return    LINEAR_TEMP(4000,  5000,  -11+273, -18+273, altitude);  // -18+273 ;
  if (altitude>=5000 && altitude<6000) return    LINEAR_TEMP(5000,  6000,  -18+273, -23+273, altitude);  // -23+273 ;
  if (altitude>=6000 && altitude<7000) return    LINEAR_TEMP(6000,  7000,  -23+273, -31+273, altitude);  // -31+273 ;
  if (altitude>=7000 && altitude<8000) return    LINEAR_TEMP(7000,  8000,  -31+273, -37+273, altitude);  // -37+273 ;
  if (altitude>=8000 && altitude<9000) return    LINEAR_TEMP(8000,  9000,  -37+273, -44+273, altitude);  // -44+273 ;
  if (altitude>=9000 && altitude<10000) return   LINEAR_TEMP(9000,  1000,  -44+273, -50+273, altitude);  // -50+273 ;
  if (altitude>=10000 && altitude<15000) return  LINEAR_TEMP(10000, 15000, -50+273, -57+273, altitude);  // -57+273 ;
  if (altitude>=15000 && altitude<20000) return  LINEAR_TEMP(15000, 20000, -51+273, -51+273, altitude);  // -51+273 ;
  if (altitude>=20000 && altitude<25000) return  LINEAR_TEMP(20000, 25000, -45+273, -45+273, altitude);  // -45+273 ;
  if (altitude>=30000 && altitude<35000) return  LINEAR_TEMP(30000, 35000, -40+273, -40+273, altitude);  // -40+273 ;
  if (altitude>=40000 && altitude<45000) return  LINEAR_TEMP(35000, 45000, -25+273, -25+273, altitude);  // -25+273 ;
  else return -22+273;
}
/**
 * Pressure at altitude
 * See http://www.engineeringtoolbox.com/air-altitude-pressure-d_462.html
 * @param altitude in meters
 *  */
uint32_t press_at_altitude(double altitude) { 
  return (uint32_t)(101325 * pow(1-2.25577e-5*altitude, 5.25588));
}


double _current_altidute = 570;

void power_on_12(){}
void power_off_12(){}
void power_on_7(){}
void power_off_7(){}
void power_on_5(){}
void power_off_5(){}
bool is_on_powerline12(){return true;}
bool is_on_powerline7(){return true;}
bool is_on_powerline5(){return true;}
void set_powerline12(bool enable){}
void set_powerline7(bool enable){}
void set_powerline5(bool enable){}

uint32_t get_millis(){
  return millis();
}

/// GPS 

void init_GPS() {
  return;
}
bool acquire_GPS(struct GPSData * gpsdata) {
  gpsdata->lon=211069;
  gpsdata->lat=678939;
  gpsdata->alt=(uint16_t) _current_altidute;//570; //m
  return true;
}
void enable_GPS(bool enable) {
  return ;
}

/// PRES
void init_PRES() {
  return;
}
uint32_t acquire_PRES() {
  return press_at_altitude(_current_altidute); //94662; // Simulator pressure at 570 mt
}
void enable_PRES(bool enable) {
  return;
}

/// OPC

void init_OPC() {
  return;
}
bool acquire_available_OPC(){return true;}
void get_latest_data_OPC(uint16_t * buf38){
  for (unsigned int i=0;i<38;i++){
    buf38[i-1]=(uint16_t)0x4140+i; 
    //buf38[i+19]=(uint16_t)0x4140+i; 
  }
  return;
} 
bool acquire_OPC(uint16_t * buf38){
  get_latest_data_OPC(buf38);
  return true;
}
void enable_OPC(bool enable) {
  return;
}

/// AIC1
void init_AIC1() {
}
uint16_t acquire_AIC1() {
  return 100;
}
void enable_fan_AIC1(bool enable){
}
void enable_AIC1(bool enable) {
  return;
}

/// AIC2

void init_AIC2() {
}
uint16_t acquire_AIC2() {
  return 100;
}
void enable_fan_AIC2(bool enable){
}
void enable_AIC2(bool enable) {
  return;
}

///VALVES

void init_VALVES(){ }
void set_VALVES(uint8_t status){
}

/// TEMP

void init_TEMP() {
  return;
}
uint16_t acquire_TEMP() {
  _current_altidute+=5.0;
  return (uint16_t) temp_at_altitude(_current_altidute)*100 ; //28300; // Last 2 digits represents decimals
}
void enable_TEMP(bool enable) {
  return;
}

/// RH 

void init_RH() {
}
uint16_t acquire_RH() {  
  return 3000; // Last 2 digits represents decimals
}
void enable_RH(bool enable) {
  return;
}

/// SD

void init_SD() {
  return;
}
void log_SD(struct StatusMessage * msg) { 
  return;
}
void enable_SD(bool enable) {
  return;
}

/// PUMP

void init_PUMP() {
}
void set_PUMP(uint16_t value) {
}

/// SYSTEM

void (*REBOOT) () = 0;
void reboot() {
  REBOOT();
}

/// COMMUNICATION

uint8_t gscom_mac [6] = GSCOM_MACADDR; 
DECLARE_GSCOM_GS_IP
DECLARE_GSCOM_OBDH_IP
EthernetUDP gscom_server;

void init_GSCOM() {
  Ethernet.begin(gscom_mac, gscom_obdh_ip);
	gscom_server.begin(GSCOM_SERVICE_PORT);
}
void GSCOM_tx(struct Message * msg) {
	gscom_server.beginPacket(gscom_gs_ip, GSCOM_SERVICE_PORT);
	//msg -> crc = calculateCrc((uint8_t *) msg, len);
  int length=0;
  MESSAGE_LENGTH(msg, length);
  gscom_server.write((uint8_t *) msg, length);
	gscom_server.endPacket();
}
bool GSCOM_rx(struct Message * msg) {
  char buffer [512];
  unsigned int length=0;
	int recvPacketSize = gscom_server.parsePacket();
	if (recvPacketSize > 0){
		gscom_server.read(buffer , 512);
    MESSAGE_LENGTH( ((struct Message *) buffer), length);
    for (unsigned int i=0; i<length && i<sizeof(struct Message); i++){
      ((char*)msg)[i] = buffer[i];
    } 
		// TODO: Check CRC
		//uint16_t recvCrc = msg -> crc;
		//msg -> crc = 0; // I assume the crc is calculated with this set to zero. TODO it's correct??
		//uint16_t calculatedCrc = calculateCRC(msg, sizeof(struct Message));
		//if(recvCrc != calculatedCrc){
		//	// Packet corrupted!! I discard it
		//	return false;
		//}
		return true;
	} else {
		return false;
	}
}
void enable_GSCOM(bool enable) {
  return;
}
