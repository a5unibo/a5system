/**
 * driver_consts.h
 * Copyright (C) A5 Unibo 
 *
 * Distributed under terms of the CC-BY-NC license.
 */

#ifndef DRIVER_CONSTS_H
#define DRIVER_CONSTS_H

#define TEMP_DS18S20_PIN 2

#define OPC_SERIAL_RX_CHAN 10
#define OPC_SERIAL_TX_CHAN 11

#define PRES_SENSOR_PIN 10 
// Command for the pressure sensor
#define PRES_ADC_READ     0b00000000
#define PRES_RESET        0b00011110
#define PRES_PROM_READ_0  0b10100000  // Factory data and setup
#define PRES_PROM_READ_1  0b10100010  // C1
#define PRES_PROM_READ_2  0b10100100  // C2
#define PRES_PROM_READ_3  0b10100110  // C3
#define PRES_PROM_READ_4  0b10101000  // C4
#define PRES_PROM_READ_5  0b10101010  // C5
#define PRES_PROM_READ_6  0b10101100  // C6
#define PRES_PROM_READ_7  0b10101110  // Serial code and crc
#define PRES_CONVERT_D1   0b01001000
#define PRES_CONVERT_D2   0b01011000

#define SD_CARD_PIN 10
#define SD_LOGFILE "mission.log"


#define RH_SENSOR_ADDR 39
#define RH_ACK 1
#define RH_NACK 0

#define GSCOM_SERVICE_PORT 4141
#define GSCOM_MACADDR     {0xde,0xad,0xbe,0xef,0xde,0xad}
#define GSCOM_IP          {192,168,100,10}
#define GSCOM_GW          {192,168,100,254}
#define GSCOM_MASK        {255,255,255,0}
#define DECLARE_GSCOM_OBDH_IP     IPAddress gscom_obdh_ip(172, 16, 18, 100);
#define DECLARE_GSCOM_GS_IP       IPAddress gscom_gs_ip(172, 16, 18, 101);

#define POWER_LINE_1_PIN  29       // Power Line 1
#define POWER_LINE_2_PIN  33       // Power Line 2
#define POWER_LINE_3_PIN  39       // Power Line 3

#endif /* !DRIVER_CONSTS_H */

