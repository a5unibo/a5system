#include <obdh_common/hal.h>
#include <obdh_common/data.h>
#include <obdh_driver/driver_consts.h>

#include <Arduino.h>
#include <math.h>
#include <obdh_driver/SPI/SPI.h>
#include <obdh_driver/SD/SD.h>
#include <obdh_driver/OneWire/OneWire.h>
#include <obdh_driver/SoftwareSerial/SoftwareSerial.h>
#include <obdh_driver/Wire/Wire.h>
#include <obdh_driver/Ethernet/Ethernet.h>
#include <obdh_driver/Ethernet/EthernetServer.h>
#include <obdh_driver/Ethernet/EthernetClient.h>


#ifdef DEBUG
  #ifndef ARDUINO_CODE
    #include <iostream>
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {std::cout<<"[-] "<<x<<std::endl;}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {std::cout<<"[+] "<<x<<std::endl;}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {std::cout<<"[!] "<<x<<std::endl;}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #else
    #ifndef DEBUG_MSG  
      #define DEBUG_MSG(x)  {Serial<<"[-] "<<x<<'\n';}
    #endif
    #ifndef INFO_MSG  
      #define INFO_MSG(x)   {Serial<<"[+] "<<x<<'\n';}
    #endif
    #ifndef ERROR_MSG  
      #define ERROR_MSG(x)  {Serial<<"[!] "<<x<<'\n';}
    #endif
    #ifndef D 
      #define D(E)          {E;}
    #endif
  #endif
#else
  #ifndef DEBUG_MSG  
    #define DEBUG_MSG(x)  { }
  #endif
  #ifndef INFO_MSG  
    #define INFO_MSG(x)   { }
  #endif
  #ifndef ERROR_MSG  
    #define ERROR_MSG(x)  { }
  #endif
  #ifndef D 
    #define D(E)          { }
  #endif
#endif

/// POWER

void power_on_12(){
  digitalWrite(POWER_LINE_1_PIN, HIGH); 
}
void power_off_12(){
  digitalWrite(POWER_LINE_1_PIN, LOW);
}
void power_on_7(){
  digitalWrite(POWER_LINE_2_PIN, HIGH); 
}
void power_off_7(){
  digitalWrite(POWER_LINE_2_PIN, LOW); 
}
void power_on_5(){
  digitalWrite(POWER_LINE_3_PIN, HIGH); 
}
void power_off_5(){
  digitalWrite(POWER_LINE_3_PIN, LOW); 
}

int FPL1 = 31;
int EPL1 = 30;
int FPL2 = 37;
int EPL2 = 35;
int FPL3 = 46;
int EPL3 = 41;

bool is_on_powerline12(){
  return digitalRead(FPL1)==LOW; 
}
bool is_on_powerline7(){
  return digitalRead(FPL2)==LOW; 
}
bool is_on_powerline5(){
  return digitalRead(FPL3)==LOW; 
}
void set_powerline12(bool enable){
  digitalWrite(EPL1, (enable?LOW:HIGH));
}
void set_powerline7(bool enable){
  digitalWrite(EPL2, (enable?LOW:HIGH)); 
}
void set_powerline5(bool enable){
  digitalWrite(EPL3, (enable?LOW:HIGH)); 
}

/// TIME

uint32_t get_millis(){
  return millis();
}

/// GPS 

SoftwareSerial SerialGPS(11,8);

int byteGPS = -1;
char linea[700] = "";
char comandoGPR[7] = "$GPGGA";
int cont = 0;
int bien = 0;
int conta = 0;
int indices[15]; // number of commas
int virgola[14] ;
boolean trovato = 0;


//*********** Conversion function from degrees to decimal
// input for ex : 4353.11
// output is an integer
long conv_deg_dec(float GPS){
  long minuti , secondi ;
  float p1, p2, p3;
  p1 = GPS - int(GPS);
  secondi = p1 * 1000000;
  p2 = int(GPS);
  p2 = p2 / 100;
  p3 = p2 - int(p2);
  minuti = p3 * 1000000;
  long  minuti2 =  minuti / 60;
  long secondi2 = secondi / 3600;
  long deg = int(p2) ;
  long deg2 = deg * 10000;
  long somma = minuti2 + secondi2 + deg2;
  return somma;
}

void init_GPS() {
  #ifndef DEBUG 
  #endif
  SerialGPS.begin(9600);
  delay(200);
  for (int i = 0; i < 700; i++){  // Initialize a buffer for received data
    linea[i] = ' ';
  }
  return;
}
bool acquire_GPS(struct GPSData * gpsdata) {
  DEBUG_MSG("DRIVER acquiring GPS");
  #ifndef DEBUG
  #endif
  float LAT, LONG, ALT;
  String latitude = "";
  String longitude= "";
  String altitude = "";
  trovato = 0;
  for (int i = 0; i < 14; i++){
    virgola[i] = 0;
  }
  for (int n = 0; n < 700; n++){
    uint32_t begin=millis();
  while (SerialGPS.available()==0){
      uint32_t curr=millis();
      if (curr-begin > 1000){ 
        ERROR_MSG("DRIVER cannot read GPS data");
        return false;
      }
    }
    byteGPS = SerialGPS.read();
    if (byteGPS == -1){   // See if the port is empty yet
    } else {
      linea[n] = byteGPS;
    }
  }
  for (int n = 0; n < 700; n++){
    for (int i = n; i < n + 7; i++){
      if (linea[i] == comandoGPR[i - n]){
        bien++;
      }
      if (bien == 6){
        int j = n;
        int v = 0;
        while (linea[j] != 13) {
          if (linea[j] == ',') {
            virgola[v] = j ;
            v++;
          }
          j++;
        }

        if (v == 14 ) {
          int l1 = virgola[1];
          l1 = l1 + 1;
          int l2 = virgola[2];
          int l3 = virgola[3];
          l3 = l3 + 1;
          int l4 = virgola[4];
          int l8 = virgola[8];
          l8 = l8 + 1;
          int l9 = virgola[9];
          for (i = l1; i < l2; i++){
            latitude += linea[i];
          }
          char floatlat[32];
          latitude.toCharArray(floatlat, sizeof(floatlat));
          LAT = atof(floatlat);
          for (i = l3; i < l4; i++){
            longitude += linea[i];
          }
          char floatlong[32];
          longitude.toCharArray(floatlong, sizeof(floatlong));
          LONG = atof(floatlong);
          for (i = l8; i < l9; i++){
            altitude += linea[i];
          }
          char floatalt[16];
          altitude.toCharArray(floatalt, sizeof(floatalt));
          ALT = atof(floatalt);
		  
          gpsdata->lat= LAT;
          gpsdata->lon=LONG;
          gpsdata->alt=ALT;
		  
		
          trovato = 1;
        }
        bien = 0;
      }
    }
    bien = 0;
    if (trovato == 1){
	DEBUG_MSG("GPS OK");
      break;
    }
  }
  return true;
}
void enable_GPS(bool enable) {
  return ;
}

/// PRES

#define PRESSURE_ADDRESS 0x76

// uint8_t PRESSURE_ADDRESS = 0x77;
uint32_t D1 = 0;
uint32_t D2 = 0;
int64_t dT = 0;
int32_t TEMP = 0;
int64_t OFF = 0;
int64_t SENS = 0;
int32_t P = 0;
uint16_t C[7];
float T2 = 0;
float OFF2 = 0;
float SENS2 = 0;

long getVal(int address, byte code){
  unsigned long ret = 0;
  Wire.beginTransmission(address);
  Wire.write(code);
  Wire.endTransmission();
  delay(20);
  // start read sequence
  Wire.beginTransmission(address);
  Wire.write((byte) 0x00);
  Wire.endTransmission();
  Wire.beginTransmission(address);
  Wire.requestFrom(address, (int)3);
  if (Wire.available() >= 3){
    ret = Wire.read() * (unsigned long)65536 + Wire.read() * (unsigned long)256 + Wire.read();
  }
  else {
    ret = -1;
    //ERROR_MSG("DRIVER PRES error in getVal");
  }
  Wire.endTransmission();
  return ret;
}


uint8_t scan_i2c(){
  byte error, address;
  int nDevices;
 
  DEBUG_MSG("Scanning I2C DEVICES");
  nDevices = 0;
  for(address = 1; address < 127; address++ ){
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error==0){
      INFO_MSG("I2C device found at address "<<((uint8_t)address));
      if (address!=RH_SENSOR_ADDR){
        return address;
      }
      nDevices++;
    }
    else if (error==4){ 
      ERROR_MSG("Unknow error at address "<<((uint8_t)address));
    }    
  }
  return 0;
}

void init_PRES() {
  // Disable internal pullups, 10Kohms are on the breakout
  //PORTC |= (1 << 4);
  //PORTC |= (1 << 5);
  D( scan_i2c(); );
  DEBUG_MSG("Initialization Pressure Sensor "<<((uint8_t)PRESSURE_ADDRESS));
  Wire.beginTransmission(PRESSURE_ADDRESS);
  Wire.write(0x1E); // reset
  Wire.endTransmission();
  delay(20);
  for (int i = 0; i < 6  ; i++){
    Wire.beginTransmission(PRESSURE_ADDRESS);
    Wire.write(0xA2 + (i * 2));
    Wire.endTransmission();
    Wire.beginTransmission(PRESSURE_ADDRESS);
    Wire.requestFrom((int)PRESSURE_ADDRESS, 6);
    delay(2);
    if (Wire.available()){
      C[i + 1] = Wire.read() << 8 | Wire.read();
    }
    else {
      ERROR_MSG("DRIVER error reading PROM 1 (PRES)"); // error reading the PROM or communicating with the device
    }
  }
  return;
}
uint32_t acquire_PRES() {
  //DEBUG_MSG("DRIVER acquiring PRES");
  D1 = getVal(PRESSURE_ADDRESS, 0x48); // Pressure raw
  D2 = getVal(PRESSURE_ADDRESS, 0x58);// Temperature raw
  // ***** CONVERSION *************
  dT = D2 - C[5] * pow(2, 8);
  TEMP = 2000 + (dT * C[6]) / pow(2, 23);
  OFF = C[2] * pow(2, 17) + (C[4] * dT) / pow(2, 6);
  SENS = C[1] * pow(2, 16) + (C[3] * dT) / pow(2, 7);
  P = (D1 * SENS / pow(2, 21) - OFF) / pow(2, 15);
  //*** SECOND ORDE TEMPERATURE COMPENSATION
  if ( TEMP < 20){
    T2 = pow(dT,2) / pow(2, 31);
    OFF2 = 61 * pow ( TEMP - 2000, 2) / pow(2, 4);
    SENS2 =2*pow(TEMP-2000, 2);
    if ( TEMP < -15){
      OFF2 = OFF2 + 15 * pow(TEMP + 1500, 2);
      SENS2 = SENS2 + 8 * pow(TEMP + 1500, 2);
    }
  } else  {
    T2 = 0;
    OFF2 = 0;
    SENS2 = 0;
  }
  TEMP = TEMP - T2;
  OFF = OFF - OFF2;
  SENS = SENS - SENS2;
  return (P);
}
void enable_PRES(bool enable) {
  return;
}

/// OPC

#define OPC_CONCENTRATION(way, chan)      ((float) (way) * _opc_conc_scale[chan%19] )
#define OPC_DNDLOGD(conc)                 ((float) (log10(conc) * 10 )/63 - 0.52 )
#define OPC_START_FRAME 181          // ord(u'µ')
#define OPC_END_FRAME   167          // ord(u'§')
#define OPC_STUPID_FRENCH '\xa3'        // ord(u'£')
#define OPC_BUFFER_SIZE (80*2)
/// Correction scale for counting to concentration conversion using LOAC
const float _opc_conc_scale [19] = { 0.02625,0.0265,0.0286875, 0.0309375, 
  0.034125, 0.0352875, 0.0254375, 0.005635, 0.009375, 0.0075, 0.009375, 
  0.013125, 0.016875, 0.020625, 0.02437 , 0.028125, 0.009375, 0.013125, 
  0.026875 };

enum ParsingPhase{ START=0, READ=1, END=2} _opc_pphase=START;
uint8_t  _opc_read_count=0;
uint8_t  _opc_start_frame_count=0;
uint8_t  _opc_end_frame_count=0;
uint8_t  _opc_read_byte_count=0;
uint8_t  _opc_byte_read=0;
uint16_t _opc_LOAC [40];
uint8_t _opc_data [80];
uint16_t _opc_buf38 [19*2];

void init_OPC() {
  _opc_pphase=START;
  _opc_read_count=0;
  _opc_start_frame_count=0;
  _opc_end_frame_count=0;
  _opc_read_byte_count=0;
  _opc_byte_read=0;
  for (unsigned int i=0;i<19*2;i++){_opc_buf38[i]=0;}
#ifndef ARDULOAC
  Serial1.begin(38400);
  Serial1.print("$RESET");     Serial1.print(OPC_STUPID_FRENCH);             // Reset LOAC software
  Serial1.print("$SETTRAMEb"); Serial1.print(OPC_STUPID_FRENCH);             // Setup LOAC's binary mode
  Serial1.print("$SETAUTOm");  Serial1.print(OPC_STUPID_FRENCH);             // Send on request
  //Serial1.print("$SETAUTOa");  Serial1.print(OPC_STUPID_FRENCH);             // Send on request
  //Serial1.print("$START05");  Serial1.print(OPC_STUPID_FRENCH);              // Send data every 5 sec
#else
  Serial1.begin(38400); //TODO set it
#endif
  return;
}
bool acquire_available_OPC(){
  bool got_message=false;
  ERROR_MSG("DRIVER acquire_available_OPC is deprecated");
  return got_message;
}
void get_latest_data_OPC(uint16_t * buf38){  
  for (unsigned int i=0; i<38;i++){
    buf38[i]=_opc_buf38[i];
  }
}
bool acquire_OPC(uint16_t * buf38){
  #ifndef ARDULOAC
  bool got_message=false;
  while(Serial1.available()>0){Serial1.read();} 
  Serial1.print("$TRAME");    Serial1.print(OPC_STUPID_FRENCH);          // Request frame
  DEBUG_MSG("DRIVER is going to read OPC messages");
  while(!got_message){ 
      uint32_t begin=millis();
      while (Serial1.available()==0){
        uint32_t curr=millis();
        if (curr-begin > 2000){ 
          ERROR_MSG("DRIVER cannot read OPC messages");
          for (unsigned int i=0; i<38;i++){
            buf38[i]=0xffff;
          }
          return false;
        }
      }
      uint8_t token = Serial1.read();
      switch (_opc_pphase){
        case START: 
          if (token==OPC_START_FRAME){
            _opc_start_frame_count++; 
            if(_opc_start_frame_count==4){
              _opc_pphase=READ;
              _opc_start_frame_count=0;
              _opc_read_byte_count=0;
            }
          } else {
            _opc_start_frame_count=0;
          } 
        break;
        case READ:
          _opc_data[_opc_read_byte_count]=token;
          _opc_read_byte_count++;
          if (_opc_read_byte_count==80){
            _opc_read_byte_count=0;
            _opc_end_frame_count=0;
            _opc_start_frame_count=0;
            _opc_pphase=END;
            for(int i=0, lc=0; i<80 && lc<40;i+=2, lc++){
              _opc_LOAC[lc]= (_opc_data[i+1]<<8) + _opc_data[i];
            }
            for (unsigned int i=2,ch=0; i<40 && ch<19; i+=2, ch++){
               buf38[ch]    =  _opc_LOAC[i];    // */ OPC_CONCENTRATION((_opc_LOAC[i]), ch)  ;  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i]), ch))   * 100;
               buf38[ch+19] =  _opc_LOAC[i+1];  // */ OPC_CONCENTRATION((_opc_LOAC[i+1]), ch);  //   OPC_DNDLOGD( OPC_CONCENTRATION((LOAC[i+1]), ch)) * 100;
            }
          } 
        break;
        case END:
          if (token==OPC_END_FRAME){
            _opc_end_frame_count++; 
            if(_opc_end_frame_count==4){
              _opc_pphase=START;
              _opc_end_frame_count=0;
              _opc_start_frame_count=0;
              got_message=true;
              DEBUG_MSG("OPC has received the message");
            }
          } else {
            _opc_end_frame_count=0;
          } 
        break;
      } 
  }
  return true;
#else
  // ArduLOAC code
  bool got_message=false;
  uint8_t counter=0;
  while(Serial1.available()>0){Serial1.read();} 
  Serial1.print("\x66");    // Request frame to ArduLOAC
  while(!got_message){ 
      uint32_t begin=millis();
      while (Serial1.available()==0){
        uint32_t curr=millis();
        if (curr-begin > 1000){ 
          ERROR_MSG("DRIVER cannot read OPC messages");
          for (unsigned int i=0; i<38;i++){
            buf38[i]=0xffff;
          }
          return false;
        }
      }
      uint8_t token = Serial1.read();
      ((uint8_t *)buf38)[counter++]=token;
      if (counter==38*2){
        return true;
      } 
  }
  return true; 
#endif
}
void enable_OPC(bool enable) {
  return;
}

/// AIC1

// I2C address for MCP3422 - base address for MCP3424
#define MCP3422_ADDRESS 0b1101010
#define ADC_config_CH1     0b10001100   // start bit , channel 1 (AIC 1); resolution 18 bits ; PGA-gain X2
#define ADC_config_CH2     0XBD   // start bit , channel 2 (AIC 2); resolution 18 bits ; PGA-gain X2
typedef struct ADC_conv{
  byte p1; // it contains the sign
  byte p2;
  byte p3;
  byte s ;  //status
} ADC_conv;
ADC_conv channel_1 , channel_2 ;

int AIC1_fan_pin =32;
int AIC2_fan_pin =32;

double conv(struct ADC_conv){
  uint8_t segno = (channel_1.p1 & 0X80) >> 7 ;
  double volt=0;
  if ( segno == 0){
    uint32_t data = uint32_t(channel_1.p1 & 0X01) << 16 | uint32_t(channel_1.p2) << 8 | channel_1.p3;
    volt = data * 15.625 ; // (15.625 * 10^-6) / 2
  } else if (segno == 1){
    uint32_t data_comp = uint32_t(channel_1.p1 & 0X01) << 16 | uint32_t(channel_1.p2) << 8 | channel_1.p3;
    uint32_t data = (data_comp ^ 0b11111111111111111) + 1 ;
    volt = data * (15.625) ; // (15.625 * 10^-6) / 2
    // E' NEGATIVO!!!!!!!
  }
  return (volt);
}

void init_AIC1() {
  // Nothing to init here 
}

uint16_t acquire_AIC1() {
  //DEBUG_MSG("DRIVER acquiring AIC1");
  int AIC_pin = A0;
  int Num = analogRead(AIC_pin);
  float voltage =Num*4.8875855;   // voltage is in millivolts
  voltage = voltage/2.2;  //2.2 is the gain of OPAMP 
   //  1mV = 1Kions/cm3
   uint16_t ions = voltage; //[kIons/cm3]
   return(ions);
}
void enable_fan_AIC1(bool enable){
  if (!enable){
    digitalWrite(AIC1_fan_pin, LOW);
  } else {
    digitalWrite(AIC1_fan_pin, HIGH);
  }
}
void enable_AIC1(bool enable) {
  return;
}

/// AIC2

void init_AIC2() {
  // Nothing to init here 
}
uint16_t acquire_AIC2() {
  //DEBUG_MSG("DRIVER acquiring AIC2");
  int AIC_pin = A1;
  int Num = analogRead(AIC_pin);
  float voltage =Num*4.8875855;   // voltage is in millivolts
  voltage = voltage/2.2;  //2.2 is the gain of OPAMP 
  //  1mV = 1Kions/cm3
  uint16_t ions = voltage; //[kIons/cm3]
  return(ions);
}
void enable_fan_AIC2(bool enable){
  if (!enable){
    digitalWrite(AIC2_fan_pin, LOW);
  } else {
    digitalWrite(AIC2_fan_pin, HIGH);
  }
}
void enable_AIC2(bool enable) {
  return;
}

///VALVES

int valves_pin=36;

void init_VALVES(){
  pinMode(valves_pin, OUTPUT);
  digitalWrite(valves_pin, LOW);
}
void set_VALVES(uint8_t status){
  if(!status){
     // Close
     digitalWrite(valves_pin,LOW);
  } else {
    // Open
    digitalWrite(valves_pin,HIGH);
  }
}


/// TEMP

//OneWire temp_ds(TEMP_DS18S20_PIN);
int DS18S20_Pin = 34; //DS18S20 Signal pin on digital 2
//Temperature chip i/o
OneWire ds(DS18S20_Pin); // on digital pin 2

void init_TEMP() {
  return;
}
uint16_t acquire_TEMP() {
  //DEBUG_MSG("DRIVER acquiring TEMP");
  
  /// Return temperature acquired from PRESSURE sensor, 
  /// you should invoke acquire_PRES to update this value
  return ((uint16_t)TEMP+27300);

  /* 
  //returns the temperature from one DS18S20 in DEG Celsius
  byte data[12];
  byte addr[8];
  if ( !ds.search(addr)){
    //no more sensors on chain, reset search
    ds.reset_search();
    ERROR_MSG("DRIVER TEMP fail read: no more sensors on chain, reset search");
    return 0xffff;
  }
  if ( OneWire::crc8( addr, 7) != addr[7]){
    //Serial.println("CRC is not valid!");
    ERROR_MSG("DRIVER TEMP fail read: invalid crc");
    return 0xffff;
  }
  if ( addr[0] != 0x10 && addr[0] != 0x28){
    //Serial.print("Device is not recognized");
    ERROR_MSG("DRIVER TEMP fail read: device not recognized");
    return 0xffff;
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1); // start conversion, with parasite power on at the end
  byte present = ds.reset();
  ds.select(addr);
  ds.write(0xBE); // Read Scratchpad
  for (int i = 0; i < 9; i++){   // we need 9 bytes
    data[i] = ds.read();
  }
  ds.reset_search();
  byte MSB = data[1];
  byte LSB = data[0];
  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;
  return ((uint16_t)TemperatureSum+273)*100;
  //delay(2000);
  */
}
void enable_TEMP(bool enable) {
  return;
}

/// RH 

void init_RH() {
  pinMode(20, OUTPUT);  // SDA pin!!!!
  digitalWrite(20, HIGH); // this turns on the sensor
}
uint16_t acquire_RH() {
  //DEBUG_MSG("DRIVER RH Humidity"); 
  byte address, Hum_H, Hum_L, Temp_H, Temp_L, _status;
  unsigned int H_dat, T_dat;
  address = 0x27;;
  Wire.beginTransmission(address);
  Wire.endTransmission();
  //delay(100);

  Wire.requestFrom((int)address, (int) 4);
  Hum_H = Wire.read();
  Hum_L = Wire.read();
  Temp_H = Wire.read();
  Temp_L = Wire.read();
  Wire.endTransmission();
  _status = (Hum_H >> 6) & 0x03;
  Hum_H = Hum_H & 0x3f;
  H_dat = (((unsigned int)Hum_H) << 8) | Hum_L;
  T_dat = (((unsigned int)Temp_H) << 8) | Temp_L;
  T_dat = T_dat / 4;
  //  *p_H_dat = H_dat;
  // *p_T_dat = T_dat;
  float RH;
  RH = (float) H_dat * 6.10e-3;
  RH = int(RH * 100); //
  return (RH);
}
void enable_RH(bool enable) {
  return;
}

/// SD

void init_SD() {
  Serial3.begin(9600);
  Serial3.println("# Time Stamp , Sate , Mode , Latitude, Longitue, Altitude, Pressure , AIC_Pos, AIC_Neg, OPC, Temp, Hum, Flags, Unused");
  return;
}
void log_SD(struct StatusMessage * msg) {
  Serial3.print(msg->life_signal);
  Serial3.print(",");
  Serial3.print(msg->state);
  Serial3.print(",");
  Serial3.print(msg->mode); 
  Serial3.print(","); 
  Serial3.print(msg->gps_data.lat); //GPS
  Serial3.print(","); 
  Serial3.print(msg->gps_data.lon); //GPS
  Serial3.print(",");  
  Serial3.print(msg->gps_data.alt); //GPS
  Serial3.print(",");  
  Serial3.print(msg->press);
  Serial3.print(",");  
  Serial3.print(msg->aic1);
  Serial3.print(",");  
  Serial3.print(msg->aic2);
  Serial3.print(","); 
  for (unsigned int i=0;i<38;i++){ 
    Serial3.print(msg->opc[i]);
    Serial3.print(";");
  }
  Serial3.print(",");
  Serial3.print(msg->temp);
  Serial3.print(","); 
  Serial3.print(msg->hr);
  Serial3.print(",");
  Serial3.print(msg->flags);
  Serial3.print(",");
  Serial3.print(msg->power);
  Serial3.println("");  
  return;
}
void enable_SD(bool enable) {
  return;
}

/// PUMP

void init_PUMP() {
  Serial2.begin(9600);
}
void set_PUMP(uint16_t value) {
  uint8_t incomingByte=0;
  Serial2.print("C"); 
  Serial2.print("01"); 
  Serial2.print(" "); 
  Serial2.print(value); 
  Serial2.println("");
  while (Serial2.available() > 0) {
    incomingByte = Serial2.read();
  }
}

/// SYSTEM

void (*REBOOT) () = 0;
void reboot() {
  REBOOT();
}

/// COMMUNICATION
uint8_t gscom_mac [6] = GSCOM_MACADDR; 
DECLARE_GSCOM_GS_IP
DECLARE_GSCOM_OBDH_IP
EthernetUDP gscom_server;

void init_GSCOM() {
  Ethernet.begin(gscom_mac, gscom_obdh_ip);
	gscom_server.begin(GSCOM_SERVICE_PORT);
}
void GSCOM_tx(struct Message * msg) {
	gscom_server.beginPacket(gscom_gs_ip, GSCOM_SERVICE_PORT);
	//msg -> crc = calculateCrc((uint8_t *) msg, len);
  int length=0;
  MESSAGE_LENGTH(msg, length);
  gscom_server.write((uint8_t *) msg, length);
	gscom_server.endPacket();
}
bool GSCOM_rx(struct Message * msg) {
  char buffer [512];
  unsigned int length=0;
	int recvPacketSize = gscom_server.parsePacket();
	if (recvPacketSize > 0){
		gscom_server.read(buffer , 512);
    MESSAGE_LENGTH( ((struct Message *) buffer), length);
    for (unsigned int i=0; i<length && i<sizeof(struct Message); i++){
      ((char*)msg)[i] = buffer[i];
    } 
		// TODO: Check CRC
		//uint16_t recvCrc = msg -> crc;
		//msg -> crc = 0; // I assume the crc is calculated with this set to zero. TODO it's correct??
		//uint16_t calculatedCrc = calculateCRC(msg, sizeof(struct Message));
		//if(recvCrc != calculatedCrc){
		//	// Packet corrupted!! I discard it
		//	return false;
		//}
		return true;
	} else {
		return false;
	}
}
void enable_GSCOM(bool enable) {
  return;
}
