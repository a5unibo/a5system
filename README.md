A5 System
=========

_Software system repository of the A5-Unibo is a student experiment team, selected to take part at the Bexus 18-19 campaign in October 2014 in Kiruna (Sweden)_

_Authors: [A5-Unibo team](http://www.the5f.com/bexus/)

_License: CC BY-NC-SA 2014_


## Project Structure

Folder structure:

      a5_obdh_common: Messages and HAL interface definitions
      a5_obdh_driver: Arduino based implementation of the HAL.
      a5_obdh_control: OBDH control code based on HAL interface.
      a5_obdh_sketch: arduino dependend HAL implementation and main sketch.
      a5_obdh_simulator: OBDH simulator that implements HAL for testing purposes.
      a5_obdh_groundstation: GS related code.

Project Process Documentation [here](https://docs.google.com/document/d/15JJ4fkUrx_1fwaQ7nfBaVcbdck-9uiy8hFGOhYdJp4k)
Task and issues [here](https://bitbucket.org/a5unibo/a5system/issues)

## Building 

#### Dependencies

* [CMake > 2.8](http://www.cmake.org/cmake/resources/software.html)
* [Arduino CMake](https://github.com/queezythegreat/arduino-cmake) 
* [Arduino IDE 1.0.5](http://arduino.cc/en/Main/Software)
* gcc-avr, binutils-avr, avr-libc, avrdude (Linux)
* LabView 2012

#### Build on Linux

1. Arduino IDE setup
    * Install `gcc-avr, binutils-avr, avr-libc, avrdude`
    * Install Arduino IDE (in `/usr/share/` or somewhere else)
    * Make sure the Arduino IDE path is present in the environment `PATH` variable
2. Go to [project page](https://bitbucket.org/a5unibo/a5system/overview) then __CLONE__
    * Then `git submodule init` and `git submodule update` for downloading and updating _arduino-cmake_ submodule
3. `cd arduino-cmake` and `./configure`
4. `cd ..` and `mkdir <BUILD_FOLDER>`  
5. `cmake -DARDUINO_SDK_PATH=<PATH_TO_ARDUINO_SDK> <PATH_TO_SOURCES>`

#### Build on Mac OS X

(the following guide might be incomplete, if you have problems ask Mattia ;) )

1. Download Arduino 1.0.x from the [website](http://arduino.cc/en/Main/Software#toc2) , extract the zip file and move the extracted `Arduino.app` in `~/Applications` (the Applications directory you see in Finder on the left).
2. Open the Terminal application
3. If you haven't already installed Homebrew, install it by launching `ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"` and do `brew update`
4. If you haven't already installed cmake, install it by launching `brew install cmake`
5. If you haven't already installed git, install it by launching `brew install git`
6. `cd` to the directory where you want to place project files (I suggest `~/git`) and clone the project repository: `git clone https://bitbucket.org/a5unibo/a5system`. Otherwise you can clone with any GUI git tool.
7. `cd` to the just cloned repository directory (for example `~/git/a5system`)
8. `git submodule init` and `git submodule update``
9. `cd arduino-cmake` and `./configure` (check if you see error messages, it should autodetect the Arduino IDE path)
10. `cd ..`, `mkdir build`, `cd build`
11. `cmake ..`

#### Build on Windows

TBD


